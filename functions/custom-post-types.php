<?php 





/**
* Registers a new post type
* @uses $wp_post_types Inserts new post type object into the list
*
* @param string  Post type key, must not exceed 20 characters
* @param array|string  See optional args description above.
* @return object|WP_Error the registered post type object, or an error object
*/
function cgp_hosts() {

    $labels = array(
        'name'                => __( 'Club/Hosts', 'mto' ),
        'singular_name'       => __( 'Club/Host', 'mto' ),
        'add_new'             => _x( 'Add New Club/Host', 'mto', 'mto' ),
        'add_new_item'        => __( 'Add New Club/Host', 'mto' ),
        'edit_item'           => __( 'Edit Club/Host', 'mto' ),
        'new_item'            => __( 'New Club/Host', 'mto' ),
        'view_item'           => __( 'View Club/Host', 'mto' ),
        'search_items'        => __( 'Search Club/Hosts', 'mto' ),
        'not_found'           => __( 'No Club/Hosts found', 'mto' ),
        'not_found_in_trash'  => __( 'No Club/Hosts found in Trash', 'mto' ),
        'parent_item_colon'   => __( 'Parent Club/Host:', 'mto' ),
        'menu_name'           => __( 'Club/Hosts', 'mto' ),
    );

    $args = array(
        'labels'                   => $labels,
        'hierarchical'        => false,
        'description'         => 'description',
        'taxonomies'          => array(),
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => null,
        'menu_icon'           => 'dashicons-businessman',
        'show_in_nav_menus'   => true,
        'publicly_queryable'  => true,
        'exclude_from_search' => false,
        'has_archive'         => true,
        'query_var'           => true,
        'can_export'          => true,
        'rewrite'             => true,
        'capability_type'     => 'post',
        'supports'            => array(
            'title', 'editor', 'author', 'thumbnail',
            'excerpt','custom-fields', 'trackbacks', 'comments',
            'revisions', 'page-attributes', 'post-formats'
            )
    );

    register_post_type( 'cpt-host', $args );
}

add_action( 'init', 'cgp_hosts' );



/**
* Registers a new post type
* @uses $wp_post_types Inserts new post type object into the list
*
* @param string  Post type key, must not exceed 20 characters
* @param array|string  See optional args description above.
* @return object|WP_Error the registered post type object, or an error object
*/
function cgp_weight_classes() {

    $labels = array(
        'name'                => __( 'Weight Classes', 'mto' ),
        'singular_name'       => __( 'Weight Class', 'mto' ),
        'add_new'             => _x( 'Add New Weight Class', 'mto', 'mto' ),
        'add_new_item'        => __( 'Add New Weight Class', 'mto' ),
        'edit_item'           => __( 'Edit Weight Class', 'mto' ),
        'new_item'            => __( 'New Weight Class', 'mto' ),
        'view_item'           => __( 'View Weight Class', 'mto' ),
        'search_items'        => __( 'Search Weight Classes', 'mto' ),
        'not_found'           => __( 'No Weight Classes found', 'mto' ),
        'not_found_in_trash'  => __( 'No Weight Classes found in Trash', 'mto' ),
        'parent_item_colon'   => __( 'Parent Weight Class:', 'mto' ),
        'menu_name'           => __( 'Weight Classes', 'mto' ),
    );

    $args = array(
        'labels'                   => $labels,
        'hierarchical'        => false,
        'description'         => 'description',
        'taxonomies'          => array(),
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => null,
        'menu_icon'           => 'dashicons-editor-justify',
        'show_in_nav_menus'   => true,
        'publicly_queryable'  => true,
        'exclude_from_search' => false,
        'has_archive'         => true,
        'query_var'           => true,
        'can_export'          => true,
        'rewrite'             => true,
        'capability_type'     => 'post',
        'supports'            => array(
            'title', 'editor', 'author', 'thumbnail',
            'excerpt','custom-fields', 'trackbacks', 'comments',
            'revisions', 'page-attributes', 'post-formats'
            )
    );

    register_post_type( 'cpt-weight-classes', $args );
}

add_action( 'init', 'cgp_weight_classes' );




 ?>