<?php 



add_action('init', function(){


    // check if the custom member role exists and
    // add it if it doesn't
    function role_exists( $role ) {

      if( ! empty( $role ) ) {
        return $GLOBALS['wp_roles']->is_role( $role );
      }
      
      return false;
    }

    if(!role_exists('member')){
        //add custom role
        $capabilities = array(
                'read'  => true
            );
        add_role( $role="member", $display_name="Member", $capabilities );
    }


} );




register_activation_hook( __FILE__, 'mto_create_member_list_db' );
function mto_create_member_list_db() {

  global $wpdb;
  $charset_collate = $wpdb->get_charset_collate();
  $table_name = $wpdb->prefix . 'mto_member_list';

  $sql = "CREATE TABLE $table_name (
    id mediumint(9) NOT NULL AUTO_INCREMENT,
    page_key varchar(255) NOT NULL,
    name varchar(255) NOT NULL,
    status varchar(255) NOT NULL,
    expiry_date varchar(255) NOT NULL,
    gender varchar(255) NOT NULL,
    city varchar(255) NOT NULL,
    state varchar(255) NOT NULL,
    country varchar(255) NOT NULL,
    age_category varchar(255) NOT NULL,
    experience_class varchar(255) NOT NULL,
    min_weight varchar(255) NOT NULL,
    max_weight varchar(255) NOT NULL,
    weight_range varchar(255) NOT NULL,
    club varchar(255) NOT NULL,
    club_id varchar(255) NOT NULL,
    membership_type varchar(255) NOT NULL,
    bouts_fought varchar(255) NOT NULL,
    UNIQUE KEY id (id)
  ) $charset_collate;";

  require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
  dbDelta( $sql );
}
add_action( 'init', 'mto_create_member_list_db' );
  









 ?>