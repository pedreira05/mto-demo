<?php

/**
 * Get all the bouts requested
 *
 * This ajax hook gets full contact conest bouts for the user
 * who's ID was passed
 *   
 * @return array bouts, pages of results
 */
function cgp_get_requested_bouts(){
    // check nonce
    $nonce = $_POST['nextNonce'];   
    if ( ! wp_verify_nonce( $nonce, 'myajax-next-nonce' ) )
        die ( 'Busted!');
        
      // get current user
      $fighter_key = $_POST['ego'];
      $fighter_id = mto_get_user_id_from_user_key($fighter_key);

    // get variables passed
    $paged = $_POST['paged'];
    $posts_per_page = $_POST['posts_per_page'];

    //pass variables to modify query

    // run query with new values
    $events_query = cgp_events_with_bouts_query($paged, $posts_per_page);
    $max_pages = $events_query->max_num_pages;
    $events = $events_query->posts;
    // Sort through results
    // $all_light_contact_bouts = array();
    // $all_full_contact_bouts = array();
    $all_bouts = array();

    // gets all events with bouts
    foreach($events as $event):
        // gets all the bouts under this event
       $bouts = get_field('bouts', $event->ID);
        if(!empty($bouts)){
          foreach ($bouts as $bout) {
            
            $nbout = new events_Query($bout, $event);

            //format results for display
            $nbout->event_end_date = cgp_format_date($nbout->event_end_date);
            $nbout->red_member['page_key'] = mto_get_user_page_key_from_user_id($nbout->red_member['ID']);
            $nbout->blue_member['page_key'] = mto_get_user_page_key_from_user_id($nbout->blue_member['ID']);

            // does current user compete in bout
            $is_fighter_in_bout = cgp_bout_has_member($nbout, $fighter_id);
            if($is_fighter_in_bout === false) continue;

            // check if is full contact bout
            // $is_full_contact = cgp_is_full_contact_bout($nbout);
            // if($is_full_contact == true)
            // $all_full_contact_bouts[] = $nbout;

            // // check if is light contact bout
            // $is_light_contact = cgp_is_light_contact_bout($nbout);
            // if($is_light_contact == true)
            // $all_light_contact_bouts[] = $nbout;
            $all_bouts[] = $nbout;

          }
        }
    endforeach;

    // setup response array
    $return['bouts'] = $all_bouts;
    
    $return['max_pages'] = $max_pages;
    $return['query'] = $events_query;
    $return['ego'] = $fighter_id;

    // generate the response
    $response = json_encode( $return );
    
    
    
    // response output
    header( "Content-Type: application/json" );
    echo $response;
    

    // IMPORTANT: don't forget to "exit"
    exit;
}
add_action( 'wp_ajax_ajax-get_requested_bouts', 'cgp_get_requested_bouts' );
add_action( 'wp_ajax_nopriv_ajax-get_requested_bouts', 'cgp_get_requested_bouts' );

/**
 * Queries all the events and builds a WP_Query object with just those
 * events that have bouts on them
 * @return obj WP_Query of allevents with bouts
 */
function cgp_events_with_bouts_query($paged="0", $posts_per_page="5" ){

      $args = array(
        'post_status'=>'publish',
        'post_type'=>array(TribeEvents::POSTTYPE),
        'posts_per_page'=>$posts_per_page,
        'eventDisplay'=>'past',
        'meta_key'=>'bouts',
        'paged'=> $paged
        
      );

      $events = new WP_Query($args );

      return $events;
}



?>