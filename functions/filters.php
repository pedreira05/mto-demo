<?php 
  

function cgp_format_date($date){
    $date = date_create($date);
    return date_format($date,"d-M-Y");
}

function calculate_days($now, $then){

     // can be passed date('d-M-Y') for now as well
    $date1 = new DateTime($now);
    $date2 = new DateTime($then);
    $interval = $date1->diff($date2);
    return $interval->days;
}

/**
 * Add custom classes to body tag
 */
add_filter( 'body_class', 'custom_class' );
function custom_class( $classes ) {
    if ( is_page_template( 'page-members.php' ) ) {
        $member_type = get_field('member_type');
        $classes[] = 'member-type-'.$member_type;
    }
    return $classes;
}


/**
 * Gets a list of all athletes by searching all in the members
 * cpt and returning only those categorized as athletes
 * @return array Post objects of each athlete
 */
function cgp_athlete_list_func(){
        // get all members from members cpt
        $param = array(
            'post_type'         => 'member'
        );
    
        $members = new WP_Query( $param );
        $athletes = array();
        
        foreach ($members->get_posts() as $member) {
            // get list of memberships this user has
            $member_type = get_field('membership_type', $member->ID);
            // if they are an athlete add them to athletes array
            if(in_array("Athlete", $member_type)):
                array_push($athletes, $member);
            endif;
           }   
        
        // Applies HTML temlpate to all athletes 
        $all_athletes = "";
        foreach ($athletes as $athlete) {
            $all_athletes .= apply_filters( 'cgp_athlete_card_html_filter', $athlete );
        }

        //reset query
        wp_reset_postdata();

        // return $athletes;
        return $all_athletes;
}

/**
 * Filter atheletes for archive page
 */
add_filter('cgp_athlete_card_html_filter', function($athlete){
    //$athlete is an array of posts from the cgp_athlete_list_func() function
    
    $id = $athlete->ID;
    $first_name = get_field('first_name', $id);
    $last_name = get_field('last_name', $id);
    $status = get_field('status', $id);
    $club = get_field('club', $id);
    $image = apply_filters( 'acf_img_filter', get_field('image',$id), 'archive_img');
    $weight = apply_filters( 'acf_weight_filter', $id );
    $experience_class = get_field('experience_class', $id);
    $record = apply_filters( 'cgp_athlete_record_filter', $id );
    $fighter_page = get_the_permalink( $id );

    $output = "";
    
    $output .= "<div class='col-md-6 fighter-archive-card'>";
    
    
    $output .= "<div class='col-sm-12'><h2 class='title-median'><a href='$fighter_page'>$first_name $last_name</a><small>" . apply_filters( 'cgp_acf_relationship_filter', $club ) . "</small></h2></div>";
    $output .= "<div class='image col-sm-12'><a href='$fighter_page'> $image </a></div>";
    // $output .= "<div class='fighter-info club col-sm-12'>" . apply_filters( 'cgp_acf_relationship_filter', $club ) . " </div>";
    $output .= "<div class='fighter-info weight col-sm-12 leadership-position'>". $weight['weight_range'] .  "</div>";
    $output .= "<div class='fighter-info status col-md-6 '> $status </div>";
    // $output .= "<div class='fighter-info experience-class col-md-4'> $experience_class </div>";
    $output .= "<div class='fighter-info record col-md-6 '> $experience_class $record </div>";

    $output .= "</div>";
    return $output;
    
} );



/**
 * Filters an ACF relationship field
 *
 * Finds all the posts in the relationship field and returns the
 * title 
 */
add_filter('cgp_acf_relationship_filter', function($field){
    //$field is an array of posts
    $items = "";
    foreach ($field as $post) {
        $id = $post->ID;

        $items.=trim("$post->post_title");
    }
    return $items;
} );




/**
 * Returns array of weigth class information
 * @param  int $id The id fo the weight class cpt
 * @return array     array containing weight info
 */
function cgp_weight_class_info($id){
    
    $weight_info = array();
            

            $min_lbs = number_format(get_field('min_lbs',$id), 1);
            $max_lbs = number_format(get_field('max_lbs',$id), 1);
            $min_lbs = number_format(get_field('min_lbs',$id), 1);
            $max_kg = number_format(get_field('max_kg',$id), 1);
            $min_kg = number_format(get_field('min_kg',$id), 1);

            $weight_range = "<span class='weight_lbs'>$min_lbs to $max_lbs lbs</span>  <span class='weight_kg'>($min_kg to $max_kg kg)</span>";
            $weight_class = get_the_title($id );
            
            $weight_info['weight_range'] = $weight_range;
            $weight_info['weight_class'] = $weight_class;
            $weight_info['weight_range_lbs'] = "$min_lbs - $max_lbs lbs";
            $weight_info['weight_range_kg'] = "$min_kg - $max_kg kg";
               
    

    return $weight_info;
                   
}


/**
 *
 * Filter to echo an image from ACF
 *    
 *
 */
function cgp_echo_image($image, $size="banner"){
    
   if(!is_array($image)):
        $image = get_field('image');
    endif;

    $size = $image['sizes'][$size];
    $alt = $image['alt'];

    return "<img src='$size' alt='$alt'/>";
    

}
add_filter( 'acf_img_filter', 'cgp_echo_image', 10, 2 );


/**
 * Assign custom taxonomy to custom post type
 *
 * Takes the value of an acf radio form and assigns
 * the post a category/term named by the value of that
 * radio button
 *             
 * @param  int $post_id post id
 */
function cgp_assign_taxonomy_to_user($post_id){
    // only do this if the post is in MTO Members type
    if("member" != get_post_type( $post_id ))
        return;

    // get the value of the member type from the form on the
    // admin screen before it is saved in the database
    $member_type =  $_POST['acf']['field_575e7061ac53d'];

    // assigns the term/category to the post for the named taxonomy
    wp_set_object_terms( $post_id, $member_type, 'members' );

}
// hook into  acf save post before data is saved in database 
// save to database happens at priority 10
add_action('acf/save_post', 'cgp_assign_taxonomy_to_user', 1);


/**
 * Sorts all members, by id, into one associative array
 * @return array associatve array with all member types and their ids
 */
function cgp_sort_members_func(){ 

        /**
         * Get all MTO members
         *
         * Returns the post object of all posts with the
         * members cpt
         * 
         * @var array all_members
         */
        $param = array(
            'post_type'         => 'member'
        );
        $all_members = new WP_Query( $param );

        // associative array of all member types
        $members = array();
        // array of post ids belonging to athletes
        $athletes = array();
        // array of post ids belonging to coaches
        $coaches = array();

        /**
         * Find the membership type held by each member and
         * push the id into the appropriate category
         */
        foreach ($all_members->get_posts() as $member) {
            // get list of memberships this user has
            $member_type = get_field('membership_type', $member->ID);
            // if they are an athlete add them to athletes array
            if(in_array("Athlete", $member_type)):
                array_push($athletes, $member->ID);
            endif;

            // if they are a Coach add them to Coach's array
            if(in_array("Coach", $member_type)):
                array_push($coaches, $member->ID);
            endif;
       } 
        // associate all athlete ids to athletes
        $members['athletes'] = $athletes;

        // associate all athlete ids to athletes
        $members['coaches'] = $coaches;


        //reset query
        wp_reset_postdata();

        return $members;
     
    

}

/**
 * Get array of all athletes 
 * @return array athlete obj
 */
function cgp_get_athletes(){
    $members = cgp_sort_members_func();
    $athletes = array();
    foreach ($members['athletes'] as $member) {
        $new_athlete = new mtoAthletes($member);
        array_push($athletes, $new_athlete);
    }
    
    return $athletes;

    wp_reset_postdata();
}

/**
 * filter to actually output list on a page
 * @return html 
 */
function cgp_list_athletes(){
    $athletes = cgp_get_athletes();
    include CGP_TEMPLATE_PATH . 'athlete-list.php';
    
}

function cgp_get_member_group($member_type="athletes"){
    $members = cgp_sort_members_func();
    $ok = "athletes";
    $list = $members[$member_type];
 
    
}

/**
 * Recursive function to generate a unique username.
 *
 * If the username already exists, will add a numerical suffix which will increase until a unique username is found.
 *
 * @param string $username
 *
 * @return string The unique username.
 */
function generate_unique_username( $username ) {

   $return_name = "";

   if ( false == username_exists( $username ) ) {
       $return_name = $username;
   } else {
       $i = 2;
       $new_username = $username . "-" . $i;
       while ( username_exists( $new_username ) != false ) {
           $new_username = $username . "-" . $i;
           $i++;
           
       }
       $return_name = $new_username;
   }

   return $return_name;

}

/**
 * Filters an ACF relationship field
 *
 * Finds all the posts in the relationship field and returns the
 * title 
 */
function cgp_get_club_name($club_id){
    //$club_id is an array of posts
    $club = get_post($club_id);

    return $club->post_title;
}



?>