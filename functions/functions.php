<?php 

/**
 * Quickly determine if the user might have an existing initial record stored in the old
 * ACF structure (initial record and uncounted experience outside mto) that wasn't transfered to the
 * new light/full contact contests repeater. 
 *
 * Returns true if old records exist while new recrods are still blank
 * @param  int $user_id user id
 * @return bool          
 */
function mto_user_has_outdated_contests_section($user_id){
  //old records
  $inital_record = get_user_meta( $user_id, 'initial_record', true );
  $uncounted_experience = get_user_meta( $user_id, 'uncounted_experience', true );
  $sum_of_old_records = (int)$inital_record + (int)$uncounted_experience;

  //new records
  $lc_contests_outside_mto = get_user_meta( $user_id, 'lc_contests_outside_mto', true );
  $fc_contests_outside_mto = get_user_meta( $user_id, 'fc_contests_outside_mto', true );
  $sum_of_contests_records = (int)$lc_contests_outside_mto + (int)$fc_contests_outside_mto;

  // if old records exist, but contests don't prepare a warning, because this means
  // the user's profile may be out of date
  if($sum_of_old_records > 0 && $sum_of_contests_records == 0){
    $bool = 1;
  } else {
    $bool = 0;
  }
  
  return $bool;
}

/**
 * Calculate if a given bout is full contact
 *
 * Takes the events_Query output as input
 *
 * Return boolean 
 */
function cgp_is_full_contact_bout($bout){
  $loc = $bout->level_of_contact;
  if($loc=="Full Contact"){
    return true;
  } else {
    return false;
  }
}

/**
 * Calculate if a given bout is light contact
 *
 * Takes the events_Query output as input
 *
 * Return boolean 
 */
function cgp_is_light_contact_bout($bout){
  $loc = $bout->level_of_contact;
  if($loc=="Light Contact"){
    return true;
  } else {
    return false;
  }
}

/**
 * Check if the given fighter fought in this bout
 *
 * Bout is the event_Query output
 * Figher id is the id of a given user
 * @return boolean 
 */
function cgp_bout_has_member($bout, $fighter_id){
  $fighters = array_keys($bout->fighters_points);

  
  $status = in_array($fighter_id, $fighters);
  return $status;
   
}

/**
 * Load all athletes to a global variable
 * if we are on a page template that requires
 * athletes.
 * 
 * @return array athletes
 */
function cgp_load_all_members(){
  $all_members = array();
  $all_athletes = array();
  $all_officials = array();
  $all_coaches = array();
  $the_members = array();
  $athlete_templates = array(
      'page-members.php'
  );

    // Do we have this information in our transients already?
    $transient = get_transient( 'mto_get_all_members' );
    
    // Return the transient if exists
    if( ! empty( $transient ) ) {

      $all_members = $transient;

    } else {

      //if there isn't already values in the global $all_members array,
      //it means this is the first request for athletes and we should find
      //them now.
     
        // get all members from members cpt
        $param = array( 'role' => 'member' );


        $members_query = new WP_User_Query( $param );

        $all_members = $members_query->get_results();

        // Save the API response so we don't have to call again until tomorrow.
        set_transient( 'mto_get_all_members', $all_members, DAY_IN_SECONDS );
      
    }

  // loop through all members and return
  // only those that are athletes
  foreach ( $all_members as $member ) {
    
    $id = $member->ID;
    $role_meta = get_user_meta($id, 'membership_type', true);
    $member_type = (is_serialized( $role_meta)) ? unserialize($role_meta) : $role_meta;
   
    if(in_array('Athlete', $member_type)){
      array_push($all_athletes, $member);
    }

    if(in_array('Coach', $member_type)){
      array_push($all_coaches, $member);
    }

    if( 
      in_array('Corner', $member_type) || 
      in_array('Official', $member_type) ){
      array_push($all_officials, $member);
    }


  }


  //populate array for return separating
  //each group of members into their category
  $the_members['athletes'] = $all_athletes;
  $the_members['officials'] = $all_officials;
  $the_members['coaches'] = $all_coaches;
  

  return $all_members;
  // return $the_members;


  
}




/**
 * Load all bouts
 * @return [type] [description]
 */
function cgp_get_all_bouts(){

      $args = array(
        'post_status'=>'publish',
        'post_type'=>array(TribeEvents::POSTTYPE),
        // 'posts_per_page'=>5,
        'eventDisplay'=>'past',
        'meta_key'=>'bouts'
        
      );

      $events = new WP_Query($args );
      $all_bouts = array();

      // gets all events with bouts
      foreach($events as $event):
          // gets all the bouts under this event
         $bouts = get_field('bouts', $event->ID);
          if(!empty($bouts)){
            foreach ($bouts as $bout) {
              
              $nbout = new events_Query($bout, $event);
              // array_push($all_bouts, $nbout);
              $all_bouts[] = $nbout;
            }
          }
      endforeach;





       return $all_bouts;
   
  
}

/**
 * Load all full contact bouts
 * @return [type] [description]
 */
function cgp_get_all_full_contact_bouts(){

      $args = array(
        'post_status'=>'publish',
        'post_type'=>array(Tribe__Events__Main::POSTTYPE),
        'posts_per_page'=>5,
        'eventDisplay'=>'past',
        'meta_key'=>'bouts',

        
      );

      $events = new WP_Query($args );
      $all_bouts = array();

      // gets all events with bouts
      foreach($events as $event):
          // gets all the bouts under this event
         $bouts = get_field('bouts', $event->ID);
          if(!empty($bouts)){
            foreach ($bouts as $bout) {
              
              $nbout = new events_Query($bout, $event);
              // array_push($all_bouts, $nbout);
              $all_bouts[] = $nbout;
            }
          }
      endforeach;





       return $all_bouts;
   
  
}




add_action( 'wp_ajax_ajax-athlete_ranking_filter', 'cgp_athlete_ranking_filter' );
add_action( 'wp_ajax_nopriv_ajax-athlete_ranking_filter', 'cgp_athlete_ranking_filter' );
function cgp_athlete_ranking_filter(){
    // check nonce
    $nonce = $_POST['nextNonce'];   
    if ( ! wp_verify_nonce( $nonce, 'myajax-next-nonce' ) )
        die ( 'Busted!');
        

    $filters = $_POST['filters'];
    $fuzzy = $_POST['fuzzy'];
    $filter_id = $_POST['filter_id'];

    $args = array(
      'post_status'=>'publish',
      'post_type'=>array(TribeEvents::POSTTYPE),
      // 'posts_per_page'=>5,
      'eventDisplay'=>'past',
      'meta_key'=>'bouts'
      
    );

    $events = new WP_Query($args );
    $all_bouts = array();

    // gets all events with bouts
    foreach($events as $event):
        // gets all the bouts under this event
       $bouts = get_field('bouts', $event->ID);
        if(!empty($bouts)){
          foreach ($bouts as $bout) {
            
            $nbout = new events_Query($bout, $event);
            // array_push($all_bouts, $nbout);
            $all_bouts[] = $nbout;
          }
        }
    endforeach;


        
        

    $ranked_bouts = array(); // will hold all bouts that match searched crieteria
    foreach ($all_bouts as $bout) {
      
      
        $gender = strtolower($bout->gender);
        $age_category = strtolower($bout->age_category);
        $experience_class = strtolower($bout->experience_class);
        $weight = $bout->weight[0]->ID;
     

      
         

        //check gender
        if(!empty($filters['gender']) && $gender != strtolower($filters['gender']))
          continue;

        //check age_category
        if(!empty($filters['age_category']) && $age_category != strtolower($filters['age_category']))
          continue;

        //check experience_class
        if(!empty($filters['experience_class']) && $experience_class != strtolower($filters['experience_class']))
          continue;

        //check weight
        if(!empty($filters['weight']) && $weight != $filters['weight'])
          continue;
        
      
        

        array_push($ranked_bouts, $bout);

        
     
    }


       
       
      $filtered_athletes['members'] = cgp_filter_rankings_query($ranked_bouts);
       


      // generate the response
      $response = json_encode( $filtered_athletes );
      
      
      
      // response output
      header( "Content-Type: application/json" );
      echo $response;
      

      // IMPORTANT: don't forget to "exit"
      exit;
  }


/**
 *
 * Ajax for athlete archive filter
 *
 */
add_action( 'wp_enqueue_scripts', 'frontend_fighter_scripts' );  
add_action( 'admin_enqueue_scripts', 'frontend_fighter_scripts' );  

function frontend_fighter_scripts() {
    // the path to the external js file
    wp_enqueue_script( 'frontend_fighter', plugin_dir_url( __DIR__ ) . 'js/min/fighter-cards-frontend-min.js', array( 'jquery' ));
 
    // utilize admin-ajax.php, Wordpress inhouse ajax.
    wp_localize_script( 'frontend_fighter', 'PT_Ajax', array(
        'ajaxurl'       => admin_url( 'admin-ajax.php' ),
        'nextNonce'     => wp_create_nonce( 'myajax-next-nonce' ),
        'ego'           => $_GET['mto_member']
      )
    );
    
}

add_action( 'wp_ajax_ajax-athlete_archive_filter', 'cgp_filter_athlete_archive_func' );
add_action( 'wp_ajax_nopriv_ajax-athlete_archive_filter', 'cgp_filter_athlete_archive_func' );
function cgp_filter_athlete_archive_func(){
    global $wpdb;

    // check nonce
    $nonce = $_POST['nextNonce'];   
    if ( ! wp_verify_nonce( $nonce, 'myajax-next-nonce' ) )
        die ( 'Busted!');
        

    $filters = $_POST['filters'];
    $fuzzy = $_POST['fuzzy'];
    $offset = intval($_POST['offset']);

    $filter_id = $_POST['filter_id'];

    if(!empty($_POST['memberType'])){
     $member_type = $_POST['memberType'];
    } else {
      $member_type = $_POST['athletes'];
    }


   

      $posts_per_page = 10; 
      $athlete_table = $wpdb->prefix."mto_member_list";

    if($fuzzy != ""){
      $the_query_total = "SELECT COUNT(id) AS total FROM $athlete_table WHERE membership_type LIKE '%".$member_type."%'
      AND (name LIKE '%".$fuzzy."%'
      OR club LIKE '%".$fuzzy."%')";

      $the_query = "SELECT * FROM $athlete_table WHERE membership_type LIKE '%".$member_type."%'
      AND (name LIKE '%".$fuzzy."%'
      OR club LIKE '%".$fuzzy."%') LIMIT $posts_per_page OFFSET $offset";
      $total_athletes_arr = $wpdb->get_results( $the_query_total );
      $athletes = $wpdb->get_results( $the_query );

      
    } else {
      // get all the filters and format them to be appended to the sql statement
      $query_filters = "";
      if(!empty($filters)){
        
        foreach ($filters as $column => $value) {
          if($column=="weight_range")
            continue;

          

            if($value)
             $query_filters .= " AND $column LIKE '$value'";
        

        }

        //append the weight range filter if applicable
        if(!empty($filters['weight_range'])){
          $weight = $filters['weight_range'];
          $query_filters .= " AND $weight BETWEEN min_weight AND max_weight";
        }
        
        
      }
      $the_query_total = "SELECT COUNT(id) AS total FROM $athlete_table WHERE membership_type LIKE '%".$member_type."%' $query_filters";
      $the_query = "SELECT * FROM $athlete_table WHERE membership_type LIKE '%".$member_type."%' $query_filters  LIMIT $posts_per_page OFFSET $offset";
      $total_athletes_arr = $wpdb->get_results( $the_query_total );
      $athletes = $wpdb->get_results( $the_query );

    }


     
    
    
    //pagintation values
    $current_page = ceil($offset / $posts_per_page) + 1;
    $total_athletes = intval($total_athletes_arr[0]->total);
    $max_num_pages = ceil($total_athletes / $posts_per_page);
    $next_page = ($current_page == $max_num_pages ) ?  "last-page" : $offset + $posts_per_page;
    $previous_page = ($offset == 0) ? "last-page" : $offset - $posts_per_page;


    //catch: current page cannot = 0
    if($current_page == 0)
        $current_page = 1;


    if(!empty($athletes)){
      $query_status = 1;
    } else {
      $query_status = 0;
    }
       
   
    $filtered_athletes = array();


  

    /**
     * If the query was initiated on the athlete filters page,
     * reduce the list of members to only athletes, then
     * apply the filters that were passed to the reduced list.
     * @var [type]
     */
      foreach ($athletes as $athlete) {
        
        array_push($filtered_athletes, $athlete);
      }


      
          
         

  

   

      

          

      if($filter_id=="athlete-filter"){
        $filtered_athletes = cgp_filter_athletes_query($filtered_athletes, $member_type); 
      }

      $return_data['members'] = $filtered_athletes;
      $return_data['query_status'] = $query_status;
      $return_data['total_athletes'] = $total_athletes;
      $return_data['max_num_pages'] = $max_num_pages;
      $return_data['next_page'] = $next_page;
      $return_data['previous_page'] = $previous_page;
      $return_data['current_page'] = $current_page;

    

    // generate the response
    $response = json_encode( $return_data );
    
    
    // response output
    header( "Content-Type: application/json" );
    echo $response;
    

    // IMPORTANT: don't forget to "exit"
    exit;
}

/**
 * Filters the results when query sent from 
 * the athlete archive  
 * @param  array $fileters   filters sent from ajax form
 * @param  array $fuzzy      search term in search
 * @return [type]             [description]
 */
function cgp_filter_athletes_query($athletes, $member_type){

 /**
  * Run the matched athletes through the filter to get the template
  */
     $data = "";
      ob_start();
     foreach ($athletes as $member) {
  
        
          if($member_type == 'official'){
              //use officials output template if this is officials page
              $data .= cgp_ajax_filter_officials_archive($member);
          } elseif($member_type == 'coach'){
              //use coaches output template if this is coaches page
              $data .= cgp_ajax_filter_coaches_archive($member);
          } else {
             // add fighters who match criteria to output
             $data .= cgp_ajax_filter_athlete_archive($member);
          }
     }
     
     
 $filtered_athletes = ob_get_contents();
 ob_end_clean();

 return $filtered_athletes; 
}
/**
 * Filters the results when query sent from 
 * the ranking archive  
 * @param  array $fileters   filters sent from ajax form
 * @param  array $fuzzy      search term in search
 * @return [type]             [description]
 */
function cgp_filter_rankings_query($ranked_bouts){
  

  ob_start(); ?>
  

  <?php

   $scores = get_athlete_scores($ranked_bouts);

   
      $rank = 1;
      foreach ($scores as $member => $member_score) :
          if(empty($member))
            continue;

          global $wpdb;



          $athlete_table = $wpdb->prefix."mto_member_list";
          $the_query = "SELECT * FROM $athlete_table WHERE id = $member";
          $athlete = $wpdb->get_results( $the_query );


          $athlete = $athlete[0];
          
          $club_name = $athlete->club;
          $url = $athlete->page_key;
          
        ?>
            <tr>
              <td class="first-name athlete-info">
                <?php echo "#$rank"; ?>
              </td>
              <td class="first-name athlete-info">
              <a href="<?php echo get_permalink() . "?mto_member=" . $url; ?>">
                 <?php echo $athlete->name ?>
                </a>
              </td>
              <td class="athlete-info gender"><?php echo $athlete->gender ?></td>
              <td class="athlete-info city"><?php echo $athlete->city ?></td>
              <td class="athlete-info state"><?php echo $athlete->state ?></td>
              <td class="athlete-info country"><?php echo $athlete->country ?></td>
              <td class="athlete-info age-category"><?php echo $athlete->age_category ?></td>
              <td class="athlete-info experience-class"><?php echo $athlete->experience_class ?></td>
              <td class="athlete-info weight-range"><?php echo $athlete->weight_range ?></td>
              <td class="athlete-info club"><?php echo  $club_name ?></td>
              <td class="athlete-info score"><?php echo $member_score ?></td>
            </tr>
        <?php
          $rank++;
      endforeach;
          
      
  $athletes = ob_get_contents();
  ob_end_clean();



  
  return $athletes;
}











/**
 *
 * Function to echo an ACF field if the field has a value
 *
 */
function cgp_echo_field($field, $default="", $wrapper="span", $label=""){
    if(""!=$label) $label = $label . ": ";
    if(""!=$wrapper):
        $prewrap = "<$wrapper>";
        $postwrap = "</$wrapper>";
    else:
        $prewrap = "";
        $postwrap = "";
    endif;

    if(get_field($field)):
        echo "$prewrap$label" . get_field($field) . "$postwrap";
    else:
        echo "$prewrap$label $default$postwrap";
    endif;

}



/**
 * Get the rank of user in specified division
 * @param  array $all_bouts all bouts from bout cpt
 * @param  array $user_info all parameters to be passed to $matched bouts
 * @return int            The user's rank 
 */
function get_athlete_rank_in_division($user_info){



   //get all bouts
   $all_bouts = cgp_get_all_bouts();
   // $all_bouts = $bouts->bouts;
  
   // // set up sort parameters from $user_info array
   $user_organization = $user_info['user_organization'];
   $user_gender = $user_info['user_gender'];
   $user_age = $user_info['user_age'];
   $user_experience = strtoupper($user_info['user_experience']);
   $user_weight = $user_info['user_weight_id'];
   $id = $user_info['id'];



    $matched_bouts = array_filter($all_bouts, function($v) use (
        $user_gender, 
        $user_age, 
        $user_organization, 
        $user_experience, 
        $user_weight
    ){
                 return $v->gender == $user_gender
                      && strtoupper($v->age_category) == strtoupper($user_age)
                      && $v->organization == $user_organization
                      && strtoupper($v->experience_class) == strtoupper($user_experience)
                      && $v->weight[0]->ID == $user_weight;
              });

    


/*
        // append meta query
        $meta_query[] = array(
            'key'       => $name,
            'value'     => $value,
            'compare'   => $operator,
        );
        // array_push($meta_query, $meta);
    endforeach;
  }
  
  
    $athletes = array();

    
   /**
     *
     * New Query based on meta queries, filters members
     * based on the key value pairs
     *
     */
    // $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
    // $paged = ($_POST['paged']) ? $_POST['paged'] : 1;
    // $args = array(
      
    //   //Type & Status Parameters
    //   'post_type'   => 'member',
    //   'posts_per_page'    => 10,
    //   'paged' => $paged, 
    //   'tax_query' => array(
    //         array(
    //           'taxonomy'         => 'members',
    //           'field'            => 'slug',
    //           'terms'            => array( 'athlete' ),
    //         )
    //       ),
    //   'meta_query'     => $meta_query,

    $scores = get_athlete_scores($matched_bouts);


    
    

    // gets user's rank.
    // searches the scores array for the user id and returns the position
    // in the array
    // add +1 to correct that the array starts on 0 index
    $rank = 1 + array_search($id, array_keys($scores));


    return $rank;


}

/**
 * Takes all the bouts entered and ranks all
 * the competing fighter based on their scores
 * and point modifications
 * @param  array $matched_bouts filtered bouts 
 * @return array                fighter id and rank
 */
function get_athlete_scores($matched_bouts){



  $scores = array();
  // get weight class ids and key indicated their order
  $weight_class_keys = get_weight_class_keys();

  //for each matched bout, find the fighters involved, add their ids to an array
  //and add the sum of their scores (plus modifications) to their id
  foreach($matched_bouts as $bout):


      $points = $bout->fighters_points; //array containing points for reach fighter this bout
      $bout_age_category = strtolower($bout->age_category); //string value
      
      

      $bout_age_category_position = array_search($bout_age_category, array_map('strtolower', $weight_class_keys)); // int value
 
     
      // loop each point, and add it to array of scores
      // key is the user id, value is the score for that bout
      foreach($points as $key => $value):
        $fighter_age_category = $bout->fighters_age_category->$key; // string value
        $fighter_age_category_position = array_search($fighter_age_category, $weight_class_keys); //int value
        $bout_age_category_position;
        //if fighter is in an age category higher than current category
        //do not add them to the calculations
        if ($fighter_age_category > $bout_age_category_position)
          continue;

          // if the fighter's id is already in the array, add the
          // score of this match to the existing value. If the fighter's
          // id is not in the array, add it and add the value
         
            // add value to existing value
            if(isset($scores[$key])){
              $scores[$key]  += $value;
            } else {
              $scores[$key]  = $value;
            }
          

         
       endforeach;
     
   endforeach;

   //find all point modifiers for this weight class
   //for this user and add the total to the user's score

   foreach($scores as $user_id => $total_points):

      //get this user's point modifier for this weight class
      $point_mod  = get_user_meta( $user_id, $key = 'point_modifier' ); // get field for 'point_modifier'
     


    
      if(!empty($point_mod)):
          
          foreach ($point_mod as $point_adjust) :
              if($point_adjust['weight_class'][0]->ID == $user_weight):
                  $total_points += $point_adjust['amount'];
              endif;
          endforeach;

          // adjust user's score with updated calculations
          $scores[$user_id] = $total_points;
      endif; 
  endforeach;
   


  // sort scores from highest to lowest
   arsort($scores,SORT_NUMERIC);
 

  
   

   return $scores;
   
}

function get_athlete_championship_title($user_info){
    // if user has a record for this division, use
    // that in place of their rank
    $user_gender = $user_info['user_gender'];
    $user_age = strtoupper($user_info['user_age']);
    $user_experience = strtoupper($user_info['user_experience']);
    $user_weight = $user_info['user_weight_id'];
    $championships = $user_info['championships'];



     $championship = array_filter($championships, function($v) use (
         $user_gender,
         $user_age,
         $user_experience,
         $user_weight 
     ){
                  return $v['gender'] == $user_gender
                       && strtoupper($v['age_class']) == $user_age
                       && strtoupper($v['experience_class']) == $user_experience
                       && $v['weight_class'][0]->ID == $user_weight;
               });
     

     return $championship;
     
}

/**
 * Assigns each wieght class a key
 * @return array all weight classes with unique index
 */
function get_weight_class_keys(){
  // assigns each weight class a key which we will
  // use to identify every weight category below it
  $age_category_object = get_field_object('field_5727f49ceed3b');
  $age_categories = $age_category_object['choices'];
  $count = count($age_categories);
  $weight_class_keys = array();
  foreach ($age_categories as $key => $value):
      $weight_class_keys[$count] = $value;
      $count--;
  endforeach;

  return $weight_class_keys;
}

/**
 * Assigns each experience class a key
 * @return array all weight classes with unique index
 */
function get_experience_class_keys(){
  // assigns each experience class a key which we will
  // use to identify every experience category below it
  $age_category_object = get_field_object('field_5727f9ba3858e');
  $age_categories = $age_category_object['choices'];
  $count = count($age_categories);
  $experience_class_keys = array();
  foreach ($age_categories as $key => $value):
      $experience_class_keys[$count] = $key;
      $count--;
  endforeach;

  return $experience_class_keys;
}


/**
 * returns each age category that is lower/younger than
 * the one passed
 *     
 * @param  string $age_category name of age category
 * @return array               list of all lower age category (names)
 */
function get_lower_age_categories($age_category){
    // get all weightclasses with their keys
    $keys = get_weight_class_keys();
    // get key of current weight class
    $this_key = array_search($age_category, $keys);
    $lower_age_categories = array();
    // the key of the lowest age category is 1
    while($this_key > 1):
        $this_key--;
        array_push($lower_age_categories, $keys[$this_key]);
    endwhile;
    return $lower_age_categories;
}


/**
 * Sorts through a given multidimensional array using
 * a given array of filters.
 *
 * Primary use includes fitering all bouts by multiple
 * criteria
 *         
 * @param  array $array   array to filter
 * @param  array $filters filters to apply in $filters['filter_key'] = 'Filter Value';
 * @return array          sorted array
 */
function cgp_filter_by_parameters($array, $filters)
{
  
  foreach ($filters as $filter => $value) 
  {
             // continue to next item in loop
             // if empty
            if( empty($value) ) 
            {
                continue;
            }
        $sorted =   array_filter($array, function($data) use ($filter, $value)
        {
          return $data[$filter] == $value;
        });

    }

  return $sorted;

}



function cgp_ajax_filter_athlete_archive($athlete){
    // $athlete = new mtoAthletes($id);
    
        
    $id = $athlete->id;
    /**
     *
     * Club information
     *
     */
    $club_name = $athlete->club;
    $club_url = get_permalink($club->ID );

    /**
     * Athlete status
     *
     * Return only 'inactive' if the status 
     * is one of the many different types of inactive
     *  
     */
    $status = $athlete->status;

    /**
     * Format expiry Date
     */
    $expiry_date = $athlete->expiry_date;
    // $expiry_date = cgp_format_date($date);

    /**
     * URL
     */
    $url = $athlete->page_key;

    ?>
    <tr><!-- news wrap -->
      
        
          
              <td class="first-name athlete-info">
              <a href="<?php echo get_permalink() . "?mto_member=" . $url; ?>">
                 <?php echo $athlete->name; ?> 
                </a>
              </td>
              <td class="athlete-info status"><?php echo $status ?></td>
              <td class="athlete-info expiry-date"><?php echo $expiry_date ?></td>
              <td class="athlete-info gender"><?php echo $athlete->gender ?></td>
              <td class="athlete-info city"><?php echo $athlete->city ?></td>
              <td class="athlete-info state"><?php echo $athlete->state ?></td>
              <td class="athlete-info country"><?php echo $athlete->country ?></td>
              <td class="athlete-info age-category"><?php echo $athlete->age_category ?></td>
              <!-- <td class="athlete-info experience-class"><?php echo $athlete->experience_class ?></td> -->
              <td class="athlete-info weight-range"><?php echo $athlete->weight_range ?></td>
              <td class="athlete-info club"><?php echo  $club_name ?></td>
          
      
      </tr><!-- news wrap end-->
    
<?php
}

/**
 * Function to output template for officials archive
 * @param  [type] $athlete [description]
 * @return [type]          [description]
 */
function cgp_ajax_filter_officials_archive($athlete){
    $id = $athlete->id;
        /**
         *
         * Club information
         *
         */
        $club_name = $athlete->club;
        $club_url = get_permalink($club->ID );

        /**
         * Athlete status
         *
         * Return only 'inactive' if the status 
         * is one of the many different types of inactive
         *  
         */
        $status = $athlete->status;

        /**
         * Format expiry Date
         */
        $expiry_date = $athlete->expiry_date;
        // $expiry_date = cgp_format_date($date);

        /**
         * URL
         */
        $url = $athlete->page_key;

   

    /**
     * Format officials course Date
     */
    $official_course_date = get_user_meta( $id, 'official_course_date', true );
    if($official_course_date){
      $official_course_date = cgp_format_date($official_course_date);
    } else {
      $official_course_date = "n/a";
    }
    /**
     * Format Judge level
     */
    $judge_level = get_user_meta( $id, 'judge_level', true );
    if($judge_level){
      $judge_level = $judge_level;
    } else {
      $judge_level = "n/a";
    }

    /**
     * Format REferee level
     */
    $referee_level = get_user_meta( $id, 'referee_level', true );
    if($referee_level){
      $referee_level = $referee_level;
    } else {
      $referee_level = "n/a";
    }

   

    /**
     * URL
     */
    $url = $athlete->page_key;


    /**
     * Format jury data
     */
    $official = get_user_meta( $id, 'jury', true );
    if($official == 1){
      $jury = "Yes";
    } else {
      $jury = "No";
    }

    ?>
    <tr><!-- news wrap -->
      
        
          
              <td class="first-name athlete-info">
              <a href="<?php echo get_permalink() . "?mto_member=" . $url; ?>">
                 <?php echo $athlete->name ?>
                </a>
              </td>
              <td class="athlete-info status"><?php echo $status ?></td>
              <td class="athlete-info expiry-date"><?php echo $expiry_date ?></td>
              <td class="athlete-info gender"><?php echo $athlete->gender ?></td>
              <td class="athlete-info city"><?php echo $athlete->city ?></td>
              <td class="athlete-info state"><?php echo $athlete->state ?></td>
              <td class="athlete-info country"><?php echo $athlete->country ?></td>
              <td class="athlete-info official-course-date"><?php echo $official_course_date ?></td>
              <td class="athlete-info judge-level"><?php echo $judge_level ?></td>
              <td class="athlete-info referee-level"><?php echo $referee_level ?></td>
              <td class="athlete-info chief-official"><?php echo $jury ?></td>
              <td class="athlete-info club"><?php echo  $club_name ?></td>
          
      
      </tr><!-- news wrap end-->
    
<?php
}


/**
 * Output archive data for coaches archive page
 */
function cgp_ajax_filter_coaches_archive($athlete){
    $id = $athlete->id;
    /**
     *
     * Club information
     *
     */
    $club_name = $athlete->club;
    $club_url = get_permalink($club->ID );

    /**
     * Athlete status
     *
     * Return only 'inactive' if the status 
     * is one of the many different types of inactive
     *  
     */
    $status = $athlete->status;

    /**
     * Format expiry Date
     */
    $expiry_date = $athlete->expiry_date;
    // $expiry_date = cgp_format_date($date);

    /**
     * URL
     */
    $url = $athlete->page_key;

    /**
     * Format expiry Date
     */
    $expiry_date = $athlete->expiry_date;
    if(!empty($athlete->expiry_date)){
      $expiry_date = cgp_format_date($athlete->expiry_date);
    } 

    /**
     * Format coaching certificate Date
     */
    $coaching_date = get_user_meta( $id, 'coaching_course_date', $single = true );
    if(!empty($coaching_date)){
      $coaching_course_date = cgp_format_date($coaching_date);
    } else {
      $coaching_course_date = "n/a";
    }

    /**
     * Coaching level
     */
    $coach_level = get_user_meta( $id, 'coach_level', $single = true );
    if(!empty($coach_level)){
      $coach_level = $coach_level;
    } else {
      $coach_level = "n/a";
    }

    ?>
    <tr><!-- news wrap -->
      
        
          
              <td class="first-name athlete-info">
              <a href="<?php echo get_permalink() . "?mto_member=" . $url; ?>">
                 <?php echo $athlete->name; ?>
                </a>
              </td>
              <td class="athlete-info status"><?php echo $status ?></td>
              <td class="athlete-info expiry-date"><?php echo $expiry_date ?></td>
              <td class="athlete-info gender"><?php echo $athlete->gender ?></td>
              <td class="athlete-info city"><?php echo $athlete->city ?></td>
              <td class="athlete-info state"><?php echo $athlete->state ?></td>
              <td class="athlete-info country"><?php echo $athlete->country ?></td>
              <td class="athlete-info coaching_course_date"><?php echo $coaching_course_date ?></td>
              <td class="athlete-info coach_level"><?php echo $coach_level ?></td>
              <td class="athlete-info club"><?php echo  $club_name ?></td>
          
      
      </tr><!-- news wrap end-->
    
<?php
}

/**
 * Order athlete archive by first name
 *
 * Alters the main queary for archive page before results
 * are loaded
 */
function order_athlete_archive_page_by_firstname( $query ) {
  
    
    // do not modify queries in the admin
    if( is_admin() ) {
        
        return $query;
        
    }
    

    // only modify queries for 'event' post type
    // if( isset($query->query_vars['post_type']) && $query->query_vars['post_type'] == 'member' ) {
    if( isset($query->query_vars['members'] ) ) {
        
        $query->set('orderby', 'meta_value');   
        $query->set('meta_key', 'first_name');   
        $query->set('order', 'ASC'); 
    }
        
    

    // return
    return $query;

}

add_action('pre_get_posts', 'order_athlete_archive_page_by_firstname');

/**
 * Show more athletes on the archive page
 */
function wpsites_query( $query ) {
if ( $query->is_page_template('page-members') && $query->is_main_query() && !is_admin() ) {
        $query->set( 'posts_per_page', 10 );
    }
}
add_action( 'pre_get_posts', 'wpsites_query' );

/**
 * Custom Pagination for  archive filter
 * @param  integer $echo [description]
 * @return [type]        [description]
 */
function cgp_pagination( $myquery ) {
  $output = '';
  $echo = 1;

  global $wp_rewrite;
  
  $myquery->query_vars[ 'paged' ] > 1 ? $current = $myquery->query_vars[ 'paged' ] : $current = 1;
  
  if( $myquery->max_num_pages > 1 ) {  
    $paginate_links_args = array(
      'base' => @add_query_arg( 'paged','%#%' ), 
      'format' => '',
      'current' => $current,
      'show_all' => true,
      'total' => $myquery->max_num_pages,
      'mid_size' => 5,
      'prev_text' => __( "&larr; PREVIOUS", "kazaz" ),
      'next_text' => __( "NEXT &rarr;", "kazaz" ),
      'type' => 'list',
      'add_args' => false,
      'add_fragment' => ''
    );
    if( !empty( $myquery->query_vars[ 's' ] ) ) $paginate_links_args[ 'add_args' ] = array( 's' => get_query_var( 's' ) );
    $output .= "<div class='row gutter'><!-- row --><div class='col-lg-12'>" . paginate_links( $paginate_links_args ) . "</div></div><!-- row end -->";
  }
  if( $echo ) echo $output;
  else return $output;
}

/**
 * Save post metadata when a post is saved.
 *
 * @param int $post_id The post ID.
 * @param post $post The post object.
 * @param bool $update Whether this is an existing post being updated or not.
 */
function save_calculated_athlete_info( $user_id ) {
    if(!user_can( $user_id, 'member' ))
      return;
   

    $athlete = new mtoAthletes($user_id);
    $experience_class = $athlete->experience_class();
    $age_category = $athlete->age_category();
    $status = $athlete->member_status();

    // - Update the post's metadata.

    if ( isset( $experience_class ) ) {
        update_user_meta( $user_id, 'calculated_experience_class', $experience_class  );
        update_user_meta( $user_id, 'calculated_age_category', $age_category  );
        update_user_meta( $user_id, 'calculated_status', $status  );
    }

    
}
add_action( 'profile_update', 'save_calculated_athlete_info', 10, 3 );
add_action( 'user_register', 'save_calculated_athlete_info', 10, 3 );

/**
 * Update user first and last name with that from ACF fields in their profile
 *
 * If the acf first and lastname fields have a value, overwrite the native WP
 * firstname and lastname  with that value
 *     
 */
function mto_update_member_name_from_acf( $user_id ){

  $firstname = get_field( 'first_name', 'user_'.$user_id );
  $lastname = get_field( 'last_name', 'user_'.$user_id );

  if(!empty($firstname)) {
    update_user_meta( $user_id, 'first_name', $firstname );
  }
  if(!empty($lastname)){
    update_user_meta( $user_id, 'last_name', $lastname );
  }
}
add_action( 'profile_update', 'mto_update_member_name_from_acf', 10, 3 );
add_action( 'user_register', 'mto_update_member_name_from_acf', 10, 3 );

/**
 * Get all registered weightclasses
 * @return array weight class id -> weight class name
 */
function cgp_get_all_weightclasses(){
  $args = array(
            'post_type'    => 'cpt-weight-classes',
            'posts_per_page' => 100
        );

$weight_classes = new WP_Query( $args );
$weight_values = array();
if ( isset($weight_classes->posts) ) :
   foreach ($weight_classes->posts as $weight) {
     $weight_values[$weight->ID]=$weight->post_title;
   }
endif;

return $weight_values;
}


/**
 * Converts user key to user ID
 *
 * Takes the user key from the mto_member param on the user profile page
 * and converts it back into the wordpress user ID. Requires the user to be in the
 * mto_member_list table
 *
 *    
 * @return int user id
 */
function mto_get_user_id_from_user_key($page_key){
  global $wpdb;
  $athlete_table = $wpdb->prefix."mto_member_list";
  $the_query = "SELECT id FROM $athlete_table WHERE page_key = $page_key";
  $user_id = $wpdb->get_results( $the_query );
  $id = $user_id[0]->id;  
  return $id;
}


function mto_get_user_page_key_from_user_id($user_id){
  global $wpdb;
  $athlete_table = $wpdb->prefix."mto_member_list";
  $the_query = "SELECT page_key FROM $athlete_table WHERE id = $user_id";
  $get_key = $wpdb->get_results( $the_query );
  $the_key = $get_key[0]->page_key;  
  return $the_key;
}


function mto_load_member_page(){

  /**
   * Detects which member was passed to the members page
   */
  if(is_page_template( 'page-members.php' )){
    $member_name = $_GET['mto_member'];
    $member = get_user_by( 'login', $member_name );
    // we have the WP_User object, now we can get the
    // member data by passing the id to our athlete query
    
  }
}
add_action( 'wp', 'mto_load_member_page' );


function cgp_email_administrators() {
    /**
     * At this point, $_GET/$_POST variable are available
     *
     * We can do our normal processing here
     */
     
    // Sanitize the POST field
    if(!empty($_POST)){
      $data = $_POST;
    } else {
      die('oops!');
    }
   

    $id = mto_get_user_id_from_user_key( $data['user_key'] );
    // Generate email content
    $email = $data['email'];
    $name = get_user_meta( $id, 'first_name', true );
    $lastname = get_user_meta( $id, 'last_name', true );
    $login_name = get_user_meta( $id, 'nickname', true );
    $referrer = $data['referrer'];

    $redirect_to = $referrer . '?mto_member=' . $data['user_key'] . '&req_profile=true';

   


    // if in the dev environment, email the developer,
    // otherwise, email the administrators

    if($env=='local'){
      $to = 'info@curtispeters.com';
    } elseif($env=='com'){
      $to = array('info@curtispeters.com', 'info@muaythaiontario.org');
    } else {

        $to = 'info@muaythaiontario.org';  
    }

     

     ob_start();
     ?>
      <table style="width:600px">
        <tr>
          <td>
            <p><strong><?php echo "$name $lastname"; ?></strong> is claiming their MTO account.</p>
            <p><?php echo $name; ?> has sent a request to claim their profile.  Complete the following steps to update <?php echo $name; ?>'s record:</p>
            
              <ol>
                <li>Log into the MTO website using the url and login credentials provided by your administrator</li>
                <li>From the dashboard, click User > All Users </li>
                <li>From the list of users, find <?php echo $name ?>'s profile (Username: <strong><?php echo $login_name; ?></strong>) and click to edit. </li>
                <li>Enter <?php echo $name ?>'s email address, <strong><?php echo $email; ?></strong>, into the <em>'Email'</em> field. </li>
                <li>Click <em>'Update User'</em> at the bottom of the page.</li>

                <li><a href="mailto:<?php echo $email; ?>?subject=Your%20MyMTOAccount%20is%20ready!&body=<?php echo urlencode($user_greeting); ?>">Email <?php echo $name; ?></a> a link to the MTO login page so they can reset their password and start using their account.</li>

              </ol>
          </td>
          
          
        </tr>
        <tr>
          <td>
            <table>
              <tr>
                <td colspan=2 style="text-align: center">
                  <strong>Summary</strong>
                </td>
              </tr>
              <tr>
                <td style="padding-right:20px;">User</td>
                <td><?php echo "$name $lastname"; ?></td>
              </tr>
              <tr>
                <td style="padding-right:20px;">User Login</td>
                <td><?php echo $login_name; ?></td>
              </tr>
              <tr>
                <td style="padding-right:20px;">User Email</td>
                <td><?php echo $email ?></td>
              </tr>
              <tr>
                <td style="padding-right:20px;">Login Url</td>
                <td><?php echo wp_login_url( '', false ); ?></td>
              </tr>
            </table>
            
          </td>
        </tr>
      </table>
     <?php
    
    $body = ob_get_clean();

    $subject = 'Request to claim account on MTO';
    $headers = array('Content-Type: text/html; charset=UTF-8');


    // Send to appropriate email
    wp_mail( $to, $subject, $body, $headers );
    

    wp_redirect( $redirect_to );

    
}
add_action( 'admin_post_nopriv_contact_admins', 'cgp_email_administrators' );
add_action( 'admin_post_contact_admins', 'cgp_email_administrators' );




/**
 * CGP depricated
 * @param  [type] $user_id [description]
 * @return [type]          [description]
 */
function mto_display_member_record_breakdown($user_id){
  
    // get all info for this athlete stored in acf
    $athlete = new mtoAthletes($user_id);

    /**
     *
     * Initial record
     *
     * Wins/loss/draw/demos by fighter on 
     * initial posting. These are non-MTO records
     *
     */

    $init_wins= $athlete->tally_record_column("wins");
    $init_losses= $athlete->tally_record_column("loss");
    $init_draws= $athlete->tally_record_column("draw");
    $init_demos= $athlete->tally_record_column("demo");



    /**
     *
     * Ongoing Record
     *
     * Wins/losses/draws/demos by figther in
     * current mto events since initiation
     *
     */
    $wins = count($athlete->get_mto_wins());
    $losses = count($athlete->get_mto_losses());
    $draws = count($athlete->get_mto_draws());

    // fights won that were hosted by organization other than MTO
    $non_mto_wins = abs(count($athlete->fights_won()) - $wins) + $init_wins;

    // fights lossed that were hosted by orginzations other than MTO
    $non_mto_losses = abs(count($athlete->fights_lossed()) - $losses) + $init_losses;

    // fights drawn that were hosted by orginzations other than MTO
    $non_mto_draws = abs(count($athlete->fights_drawn()) - $draws) + $init_draws;



    /**
     *
     * Demos will be zero for all athletes for now
     * since there isn't an option for demos as a
     * fight type in the backend
     *
     */

    $demos = "0";

    /**
     *
     * Totals
     *
     * sum of wins/loss/draw/demos between fighter's initial
     * record and what they have accumulated since becoming
     * a member
     *
     */

    $total_wins = $init_wins + $wins;
    $total_losses = $init_losses + $losses;
    $total_draws = $init_draws + $draws;
    $total_demos = $init_demos + $demos;



  ob_start();
  include(locate_template( 'content-record-breakdown.php' )); 
  $fighter_record = ob_get_contents();
  ob_clean();

return $fighter_record;

}



function my_profile_update($user_id) {
    // Do something
    $date = date('Y-m-d');

    update_usermeta( $user_id, 'last_updated', cgp_format_date($date) );
}
add_action( 'profile_update', 'my_profile_update', 10, 2 );


/**
 * Adds to admin footer the date user profile was last updated
 *
 * Views the user meta for the user currently being viewed and
 * returns the value of the 'last_updated' field. Then prints the
 * results in the admin footer.
 *
 * 
 */
function add_date_last_updated_notice_to_admin_footer(){
  $admin_page = get_current_screen();
  $screen = $admin_page->id;  

  if($screen == "user-edit"){
    $user_id = $_GET['user_id'];
    $last_updated = get_user_meta( $user_id, 'last_updated', true );

    echo "<p class='profile-update-notice'>Profile last updated on $last_updated<p>";
  }
  
}
add_action( 'in_admin_footer', 'add_date_last_updated_notice_to_admin_footer' );


/**
 * Add admin header for Members screen
 *
 * Adds a section at the top of admin screens for members that shows the 
 * login button and additional details like their name
 */
add_action('in_admin_header', function(){
  if(current_user_can( 'member' ) ):
    global $current_user;
    get_currentuserinfo();
      $firstname = $current_user->user_firstname;
      $lastname = $current_user->user_lastname;
?>
 <div class="member-admin-bar-container">
  <div class="member-admin-bar">
     <?php echo "$firstname $lastname"; ?>
     <a href="<?php echo wp_logout_url( $redirect = '' ) ?>" class="btn btn-secondary">Logout</a>
  </div>
 </div>
<?php
endif;
});


/**
 * Add admin notice section for admin screen
 *
 * Adds a section at the top of admin screens for members that shows notices we
 * wish to present to the user
 */
add_action('in_admin_header', function(){
  if(!current_user_can( 'member' ) ) return;
    global $current_user;
    get_currentuserinfo();
    $user_id = $current_user->ID;
    $athlete = new mtoAthletes($user_id);

    $notice = "";

    // display notice to update contests record if applicable
    $contest_out_of_date = $athlete->has_outdated_contests_data;
    if($contest_out_of_date == 1){
      $notice .= '<p class="mto-notice">Your external contests information may be out of date. Please be sure to update your record under the "Athlete Information" section to re-enable your status.</p>';
    }
?>
  <?php if($notice != ""): ?>
   <div class="member-notice-bar-container">
    <div class="member-notice-bar">
       <?php echo $notice; ?>
    </div>
   </div>
  <?php endif; ?>
  <?php

});
