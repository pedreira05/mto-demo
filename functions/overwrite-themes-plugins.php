<?php 

// Conditional function code for 'member' User Role
function is_member_user(){
    if( current_user_can('member') ) return true;
    else return false;
 }

// Redirect 'member' User Role to the User edit prodile on WooCommerce's My Account
// So when he get looged or it register too
 add_filter('template_redirect', 'wp_member_my_account_redirect' );
function wp_member_my_account_redirect() {
    if( is_member_user() && is_account_page() )
        wp_redirect( get_edit_profile_url( get_current_user_id() ) );
}

// Prevent automatic woocommerce redirection for 'member' User Role 
add_filter( 'woocommerce_prevent_automatic_wizard_redirect', 'wc_member_auto_redirect', 20, 1 );
function wc_member_auto_redirect( $boolean ) {
    if( is_member_user() )
        $prevent_access = true;
    return $boolean;
}

// Allow 'member' User Role to  view the Dashboard
add_filter( 'woocommerce_prevent_admin_access', 'wc_member_admin_access', 20, 1 );
function wc_member_admin_access( $prevent_access ) {
    if( is_member_user() )
        $prevent_access = false;

    return $prevent_access;
}

// Show admin bar for 'member' User Role
// add_filter( 'show_admin_bar', 'wc_member_show_admin_bar', 20, 1 );
function wc_member_show_admin_bar( $show ) {
    if ( is_member_user() )
        $show = true;
    return $show;
}


 ?>