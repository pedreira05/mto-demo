<?php 

/**
 * Checks all wordpress users and updates the corresponding member
 * in the member table with all changes
 * 
 */
function mto005_update_user_cron(){

	global $wpdb;

	$charset_collate = $wpdb->get_charset_collate();
  
	// table that holds the cron schedule
	$mto_update_user_cron_schedule_table = $wpdb->prefix . "mto_update_user_cron_schedule";
	$user_table = $wpdb->prefix . "users";



	// create the table if it doesn't exist
	$create_user_cron_schedule_table = "CREATE TABLE $mto_update_user_cron_schedule_table (
	    id mediumint(9),
	    next_batch_schedule DATETIME,
	    cycle_offset mediumint(9)
	);";
	// maybe_create_table( $mto_update_user_cron_schedule_table, $create_user_cron_schedule_table );

	if($wpdb->get_var("SHOW TABLES LIKE '$mto_update_user_cron_schedule_table'") != $mto_update_user_cron_schedule_table) {
		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
  		dbDelta( $create_user_cron_schedule_table );

  		$wpdb->query( $wpdb->prepare( 
  		  "
  		    INSERT INTO $mto_update_user_cron_schedule_table
  		    ( id,  next_batch_schedule, cycle_offset )
  		    VALUES ( %d, %s, %d)
  		  ", 
  		    array(
  		      1,
  		      NULL, 
  		      0
  		    ) 
  		) );
	}


	// get cron values
	$get_cron_table = $wpdb->get_results( "SELECT * FROM $mto_update_user_cron_schedule_table" );
	$cron_values = $get_cron_table[0];



	//get users
	$users = $wpdb->get_results( "SELECT ID FROM $user_table" );
	$user_count = count($users);

	// pop vals
	$next_batch_run = $cron_values->next_batch_schedule;
		
	if(!empty($cron_values->cycle_offset)){
		$cycle_offset = $cron_values->cycle_offset;
	} else {
		$cycle_offset = 0;
	}
		

	$limit = 300; // total number of results to return each time

	$now = date("Y-m-d");

	

	// check that the "next_batch" date has elapsed or that it is not set 
	if(!empty($next_batch_run) && strtotime($now) < strtotime($next_batch_run) )
		return;



	// check that the offset is less than total number of users 
	if($cycle_offset < $user_count){

		$user_ids = $wpdb->get_results( "SELECT ID FROM $user_table LIMIT $limit OFFSET $cycle_offset");

		foreach ($user_ids as $this_user) {
			$user_id = $this_user->ID;
			cgp005_add_updated_user_to_member_table($user_id);
		}

		// increase offset by the number of users processed
		$cycle_offset += $limit;
	}

	// if we have cycled through all the users, set next batch date and reset all values
	if($cycle_offset > $user_count){
		
		// we've cycled through all users, reset the clock for the next update to 24 hours from now
		$date = new DateTime(date("Y-m-d"));
		$date->modify('+1 day');
		$next_batch_run = $date->format('Y-m-d');

		$wpdb->query( $wpdb->prepare(
			"UPDATE $mto_update_user_cron_schedule_table
			SET `next_batch_schedule`=%s,
				`cycle_offset`=%d
				WHERE `id`=1",
			array(
				$next_batch_run,
				0
			)

		) );


	} else {
		// otherwise save current batch state in database for next loop 

		$wpdb->query( $wpdb->prepare(
			"UPDATE $mto_update_user_cron_schedule_table
			SET `cycle_offset`=%d
			WHERE `id`=1",
			$cycle_offset
			

		) );

	}


}
add_action( 'mto_delete_user_cron', 'mto005_delete_user_cron' );



function cgp_update_all_fighters_records_from_events(){
	// get events
	// get figthers records
}


 ?>