<?php 


function user_profile_fields_disable() {
 
    global $pagenow;
 
    // apply only to user profile or user edit pages
    if ($pagenow!=='profile.php' && $pagenow!=='user-edit.php') {
        return;
    }
 
    // do not change anything for the administrator
    if (current_user_can('administrator')) {
        return;
    }
 
    add_action( 'admin_head', 'user_profile_fields_disable_css' );
    add_action( 'admin_footer', 'user_profile_fields_disable_js' );
 
}
add_action('admin_init', 'user_profile_fields_disable');
 
 
/**
 * Disables selected fields in WP Admin user profile (profile.php, user-edit.php)
 */
function user_profile_fields_disable_js() {
?>
	
    <script>
        jQuery(document).ready( function($) {

        	// setup sections to be targeted by adding unique
        	// class to each section
            $(document).find('h2').each(function(){
            	var section = $(this).html().toLowerCase().replace(/ /g, '-');
            	var sectionClass = section + "-section";
            	$(this).addClass(sectionClass);
            	$(this).next('.form-table').addClass(sectionClass);
            });

            //can't remove user nickname because it throws wordpress errors.
            // Instead, move it to a visible section and just hide it
            $('.user-nickname-wrap').appendTo('.account-management-section');
            $('.user-nickname-wrap').hide();

            //hide unneeded sections
            $('.personal-options-section').remove();
            $('.name-section').remove();
            $('.about-yourself-section').remove();
            $('.user-url-wrap').remove();
            $('.user-author_email-wrap').remove();
            $('.user-author_facebook-wrap').remove();
            $('.user-author_twitter-wrap').remove();
            $('.user-author_linkedin-wrap').remove();
            $('.user-author_dribble-wrap').remove();
            $('.user-author_gplus-wrap').remove();
            $('.user-author_whatsapp-wrap').remove();
            $('.user-author_custom-wrap').remove();
            $('.collection-form-field').remove();

            // remove titles
            $('h2.contact-info-section').remove();
            $('h2.account-management-section').remove();
            $('h2.member-section').remove();

            // make the entire section visible again
            $('#profile-page').show();

            //remove admin bar
            $('#wpadminbar').remove();
            // remove help menu from dashboard
            $('#screen-meta-links').remove();
            
        });
    </script>
<?php
}

function user_profile_fields_disable_css(){
	// initially hides the entire screen until
	// after the javascript is run, thereby eliminating
	// the flicker
	?>
	<style type="text/css">
	  #profile-page,
	  .update-nag,
	  .updated settings-error notice is-dismissible,
	  .updated settings-error, 
	  .notice is-dismissible,
	  .notice,
	  .updated,
	  #wpfooter,
      #screen-meta-links,
      #wpadminbar
	  { display:none; }

	</style>
	<?php
}


/* Remove the "Dashboard" from the admin menu for non-admin users */
function remove_menus(){
  if(current_user_can('member')){
    remove_menu_page( 'index.php' );
  }
}
add_action( 'admin_menu', 'remove_menus' );

/* Prevent users from accessing the dashboard manually */
function redirect_from_dashboard(){
     $screen = get_current_screen();

     if ( $screen->base == 'dashboard' && current_user_can('member')){
        wp_redirect( 'profile.php' );
    }

}
add_action( 'current_screen', 'redirect_from_dashboard' );


/**
 * Prevent members from seeing the admin menu bar ontop the screen
 */
function hide_admin_bar_for_members(){
  if(current_user_can('member')){
    show_admin_bar(false);

  }
}
add_action( 'init', 'hide_admin_bar_for_members' );



/**
 * Make emergency contact fields mandatory for member role only
 *
 * Custom validation that checks if the emergency contact field is filled.
 * If user role is member and it isn't filled, an error is thrown
 */
function my_acf_validate_value( $valid, $value, $field, $input ){
    
    // bail early if value is already invalid
    if( !$valid ) {
        
        return $valid;
        
    }
    
    
    if(current_user_can( 'member' )){
        if( empty($value)  ) {
            
            $valid = 'Emergency contact must be fully filled';
            
        }
    }
    
    
    // return
    return $valid;
    
    
}
add_filter('acf/validate_value/name=emergency_contact_first_name', 'my_acf_validate_value', 10, 4);
add_filter('acf/validate_value/name=emergency_contact_last_name', 'my_acf_validate_value', 10, 4);
add_filter('acf/validate_value/name=emergency_contact_email', 'my_acf_validate_value', 10, 4);
add_filter('acf/validate_value/name=emergency_contact_phone', 'my_acf_validate_value', 10, 4);
add_filter('acf/validate_value/name=relationship_to_member', 'my_acf_validate_value', 10, 4);

?>