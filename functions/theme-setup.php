<?php 
$namespace = "mto";



/**
*
* Adds page name (slug) to the classes in the li that wrap main navigation links
*
**/

function add_slug_class_to_menu_item($output){
    $ps = get_option('permalink_structure');
    if(!empty($ps)){
        $idstr = preg_match_all('/<li id="menu-item-(\d+)/', $output, $matches);
        foreach($matches[1] as $mid){
            $id = get_post_meta($mid, '_menu_item_object_id', true);
            $slug = basename(get_permalink($id));
            $output = preg_replace('/menu-item-'.$mid.'">/', 'menu-item-'.$mid.' menu-item-'.$slug.'">', $output, 1);
        }
    }
    return $output;
}
add_filter('wp_nav_menu', 'add_slug_class_to_menu_item');

/**
*
* Add Dashicons (wordpress icons) to front end
*
**/
add_action( 'wp_enqueue_scripts', function(){
    wp_enqueue_style( 'dashicons' );
    // enque our main theme style after the dashicons
    // wp_enqueue_style( 'gutchess-icons', get_stylesheet_uri(), array( 'dashicons' ), '1.0' );

});


/**
*
* Custom thumbnail sizes
*
**/
function cgp_custom_thumbs(){
    add_image_size( 'content_block', 800, null, true );
    add_image_size( 'member_profile', 630, 960, true );
}
add_action( 'after_setup_theme', 'cgp_custom_thumbs' );





/**
 *
 * Move Yoast SEO fields to bottom of admin screens
 *
 * Conditionally checks if the Yoast SEO plugin is installed
 * and then moves it to the bottom of admin pages.  If this
 * doesn't work start by checking that the plugin path to wp-seo.php
 * is current and that the wpseo_metabox_prio hook is  still being used
 * in this version of the plugin
 *
 */
function cgp005_move_yoast_to_bottom(){
// Check if Yoast is installed by checking for the metabox filter
if(is_plugin_active( 'wordpress-seo/wp-seo.php')):
    function yoasttobottom() {
        return 'low';
    }
    add_filter( 'wpseo_metabox_prio', 'yoasttobottom');
endif;
}
add_action('admin_init', 'cgp005_move_yoast_to_bottom' );


/**
 *
 * Add default taxonomies for member roles cpt
 *
 * 
 * Checks if there are any terms in the member roles
 * taxonomy, and if it is empty, assume it is the inital
 * setup and set the default list of taxonomies for the
 * member roles cpt by adding list supplied in
 * the array on wp init
 *
 */
function set_member_roles(){
    $terms = get_terms('members', array('hide_empty' => false) );
    
    if(empty($terms)):
        $roles = array(
                    'Athlete',
                    'Coach',
                    'Corner',
                    'Official',
                    'Recreational'
                );

        foreach ($roles as $role) {
            wp_insert_term( $role, 'members' );
        }
    endif;
}
add_action( 'wp_loaded', 'set_member_roles' );

 ?>