<?php 

/**
* Athletes
*/
class mtoAthletes extends mtoMembers
{
    
    function __construct($id)
    {
        // call the construct of my parent class
        parent::__construct($id);

        //now the properties unique to this class
        $this->athlete_upgrade  = get_user_meta($id, 'athlete_upgrade', true);
        $this->athlete_upgrade_object  = get_field_object($id, 'athlete_upgrade');
        $this->experience_class  = get_user_meta($id, 'experience_class', true);
        $this->vsv_completed  = get_user_meta($id, 'vsv_completed', true);
        $this->background_check_completed  = get_user_meta($id, 'background_check_completed', true);

        /**
         * Process initial record
         *
         */
        $initial_record_array = array();
        $sub_field_values = array('wins','loss','draw','demo');
        $repeater_value = get_user_meta($id, 'initial_record', true);
        if ($repeater_value) {
          for ($i=0; $i<$repeater_value; $i++) {
            foreach($sub_field_values as $sub_field){
                $meta_key = 'initial_record_'.$i.'_'.$sub_field;
                $sub_field_value = get_user_meta($id, $meta_key, true);
                $initial_record_array[$i][$sub_field] = $sub_field_value;
            }
          }
        }
        

        $this->initial_record = $initial_record_array;

    

        /**
         * Process uncounted experience record
         *
         * Speed up in refactor by pulling directly from user_meta instead of the
         * acf get_field();
         */
        $this->uncounted_experience = get_field('uncounted_experience', 'user_'.$id);


        /**
         * Process Full contact Contests Outside MTO
         *
         */
        $full_contact_contests_outside_mto = array();
        $fccom_sub_field_values = array('fc_contest_date','fc_contest_sport','fc_bout_sanction','fc_contest_event', 'fc_contest_location', 'fc_contest_weight', 'fc_bout_opponent', 'fc_contest_result');
        $fccom_repeater_value = get_user_meta($id, 'fc_contests_outside_mto', true);
        if ($fccom_repeater_value) {
          for ($i=0; $i<$fccom_repeater_value; $i++) {
            foreach($fccom_sub_field_values as $sub_field){
                $meta_key = 'fc_contests_outside_mto_'.$i.'_'.$sub_field;
                $sub_field_value = get_user_meta($id, $meta_key, true);
                $full_contact_contests_outside_mto[$i][$sub_field] = $sub_field_value;
            }
          }
        }
        $this->fc_contests_outside_mto = $full_contact_contests_outside_mto;

        /**
         * Process Light contact Contests Outside MTO
         *
         */
        $light_contact_contests_outside_mto = array();
        $lccom_sub_field_values = array('lc_contest_date','lc_contest_sport','lc_bout_sanction','lc_contest_event', 'lc_contest_location', 'lc_contest_weight', 'lc_bout_opponent', 'lc_contest_result');
        $lccom_repeater_value = get_user_meta($id, 'lc_contests_outside_mto', true);
        if ($lccom_repeater_value) {
          for ($i=0; $i<$lccom_repeater_value; $i++) {
            foreach($lccom_sub_field_values as $sub_field){
                $meta_key = 'lc_contests_outside_mto_'.$i.'_'.$sub_field;
                $sub_field_value = get_user_meta($id, $meta_key, true);
                $light_contact_contests_outside_mto[$i][$sub_field] = $sub_field_value;
            }
          }
        }
        $this->lc_contests_outside_mto = $light_contact_contests_outside_mto;

        $this->has_outdated_contests_data = mto_user_has_outdated_contests_section($id);
        

        $this->medical_expiry_date = get_user_meta($id, 'medical_expiry_date', true);
        $this->lab_test_expiry_date = get_user_meta($id, 'lab_test_expiry_date', true);
        $this->reinstated_professional = get_user_meta($id, 'reinstated_professional', true);

        /**
         * Process point modifier
         *
         * Speed up in refactor by pulling directly from user_meta instead of the
         * acf get_field();
         */
        $this->point_modifier = get_field('point_modifier', 'user_'.$id);
        $this->min_weight_kg = get_user_meta($id, 'min_weight_kg', true);
        $this->max_weight_kg = get_user_meta($id, 'max_weight_kg', true);
        $this->min_weight_lbs = get_user_meta($id, 'min_weight_lbs', true);
        $this->max_weight_lbs = get_user_meta($id, 'max_weight_lbs', true);
        $this->championships = get_field('championship', 'user_'.$id);
        $this->suspensions = get_field('suspensions', 'user_'.$id);
        $this->ring_name = get_user_meta($id, 'ring_name', true);

        /**
         * Officials data
         */
        // coaching
        $this->coaching_course_date = get_user_meta($id, 'coaching_course_date', true);
        $this->coach_level = get_user_meta($id, 'coach_level', true);
        $this->concussion_safety_date = get_user_meta($id, 'concussion_safety_date', true);

        //officials
        $this->official_course_date = get_user_meta($id, 'official_course_date', true);
        $this->judge_level = get_user_meta($id, 'judge_level', true);
        $this->referee_level = get_user_meta($id, 'referee_level', true);
        $this->jury = get_user_meta($id, 'jury', true);
        $this->administrator = get_user_meta($id, 'administrator', true);
        $this->officials_shirt_size = get_user_meta($id, 'officials_shirt_size', true);
        $this->officials_shirt_received = get_user_meta($id, 'officials_shirt_received', true);
        
        $this->page_key = get_user_meta( $id, 'page_key', true );
    }
    /**
     * Calculate the expiry date for the athletes medical check
     *
     * Given a specific date, this function calculates one year in the future
     *         
     * @return string date one year from the one given
     */
    public function background_check_expiry_date(){
        $background_check_completed = $this->background_check_completed;
        $expiry_date = date('Ymd', strtotime('+1 year', strtotime($background_check_completed)) );
        return $expiry_date;
    }


    /**
     * Calculate member's status
     *
     * Checks the member's expiry date, compares 
     * it with today's date and sets the relevant
     * status (active, inactive) based on the results.
     * 
     * Checks if athlete has a suspension and sets status
     * to suspended if one exists in the future
     * 
     * Uses member status from Admin if one is set
     * 
     * @return string members tatus
     */
    public function member_status(){
        $status_override = $this->status;
        $expiry_date = strtotime($this->expiry_date);
        $suspensions = $this->suspensions;
        $now = strtotime(date('d-M-Y'));

        if($status_override):
            $status = $status_override;
        elseif($expiry_date && $now <= $expiry_date):
         // If expiry date is in the future, status = "active" on member profile page
            $status = "active";
        elseif($expiry_date && $now > $expiry_date):
            // - If expiry date is in the past, status = "inactive" on member profile page
            $status = "inactive";
        endif;

        if($suspensions):
            
            foreach($suspensions as $suspension):
                if($now < strtotime($suspension['end_date'])):
                // - If a suspension date is set in the future, 
                // status is "suspended" on member profile page
                    $status = "suspended";
                // and exit loop if a future suspension is found
                    continue;
                endif;
            endforeach;
           
            
        endif;


        return $status;

    }

    /**
     * Automatically calculate fighter's experience class
     *
     * Takes fighter's current record and finds it within
     * array of predefined record->experience class values, then 
     * returns the name. This function deals only with numeric
     * keys, the strings (names of the experience classes) are controlled
     * via acf.
     * 
     * @return string name of experience class
     */
    public function experience_class(){
        $record = $this->total_fights(); 

        //all the experience classes this fighter competed in
        $experience_class_fought = $this->experience_classes_fought_in();

        // Experience class entered in the administration page
        $experience_override = $this->experience_class;

        //array of experience class key/values
        $experience_class_keys = get_experience_class_keys(); 

        //variable for user record category int
        $user_experience_cateogry = "";

        //
        $current_experience_key = array_search($experience_override, $experience_class_keys);
        
        /**
         * If the user has competed in an Open class fight,
         * then set their current experience class as A/Open
         */
        if(in_array("O", $experience_class_fought)):
            return $user_experience_cateogry = "A/Open";
        endif;
        

        /**
         * array containing record range for each experience class category
         * where each key is a numeric value that corresponds to
         * a title record range, and the array value is the range of
         * records associated with that record category
         * @var array
         */
        $record_cats = array(
                '1'     =>      range('0','3'),
                '2'     =>      range('4', '9'),
                '3'     =>      range('10','95')
                // '4'     =>      range('20','95')

            );
      


        /**
         * searches record ranges for a specified record and
         * returns the numeric key that corresponds to 
         * the record class it belongs to
         * @var int
         */
        $user_record_cateogry_key = 1;
        foreach ($record_cats as $key => $value) {
            
            
            if(in_array($record, $value)):
                $user_record_cateogry_key = $key;
            endif;

        }


        //if current experience key is higher than that calculated, then use the
        //the value in experience override as user's experience class.
        if($current_experience_key && $current_experience_key > $user_record_cateogry_key ):
            $user_record_cateogry = $experience_override;
        else:
            /** @var string user record class */
            $user_record_cateogry = $experience_class_keys[$user_record_cateogry_key];
        endif;

        return $user_record_cateogry;
    }

    /**
     * Automatically calculate fighter's age category
     *
     * Takes fighter's current age and finds it within
     * array of predefined age->age category values, then 
     * returns the name. This function deals only with numeric
     * keys, the strings (names of the age categories) are controlled
     * via acf.
     * 
     * @return string name of age cateogry
     */
    public function age_category(){
        $age = $this->age();

        $weight_class_keys = get_weight_class_keys(); //array of weight class key/values
        $user_age_cateogry = ""; //variable for user age category int
        $user_age_cateogry_key = 0; // define variable

        /**
         * array containing age range for each age category
         * where each key is a numeric value that corresponds to
         * a title age range, and the array value is the range of
         * ages associated with that age category
         * @var array
         */
        $age_cats = array(
                '1'     =>      array('8','9'),
                '2'     =>      array(10, 11),
                '3'     =>      array(12,13),
                '4'     =>      array(14,15),
                '5'     =>      array(16,17),
                '6'     =>      range('18','40'),
                '7'     =>      range('41','55')

            );

        /**
         * searches age ranges for a specified age and
         * returns the numeric key that corresponds to 
         * the age class it belongs to
         * @var int
         */
        foreach ($age_cats as $key => $value) {
            if(in_array($age, $value))
                $user_age_cateogry_key = $key;
        }

        /**
         * Check if the category key is more than 0,
         * this is the case when a date of birth is entered
         * in admin. If it checks out, assign the age category,
         * otherwise, set a message stating it is undefined.
         */
        if($user_age_cateogry_key != 0 ):
            /** @var string user age class */
            $user_age_cateogry = $weight_class_keys[$user_age_cateogry_key];
        else:
            $user_age_cateogry = "undefined";
        endif;

        return $user_age_cateogry;
    }

    /**
     * Return yes or no if date exists for reinstated professional
     *
     * Checks that a proper date format is returned and prints 'Yes'
     * if it is.
     * 
     * @return boolean Yes or no
     */
    public function is_reinstated_professional(){
        if(1 != $this->reinstated_professional):
            $bool = "Yes";
        else:
            $bool = "No";
        endif;

        return $bool;

    }

    /**
     * The weight range for athletes in lbs
     *
     * Collects the max and min weight of the fighter as 
     * recorded on the user edit screen
     *
     * 
     * @return array contains max and min weight in lbs and kg
     */
    public function weight_range_lbs(){
        if(null==$this->min_weight_lbs || null==$this->max_weight_lbs)
            return;
        $range = number_format($this->min_weight_lbs, 1) . " - " . number_format($this->max_weight_lbs, 1);
        return $range;
    }

    /**
     * The weight range for athletes in kg
     *
     * Collects the max and min weight of the fighter as 
     * recorded on the user edit screen
     *
     * 
     * @return array contains max and min weight in lbs and kg
     */
    public function weight_range_kg(){
        if(null==$this->min_weight_kg || null==$this->max_weight_kg)
            return;
        $range = number_format($this->min_weight_kg, 1) . " - " . number_format($this->max_weight_kg, 1);
        return $range;
    }

    /**
     * Receives an associative array of all initial
     * for athlete across all disciplines, then isolates
     * the records into one associative array containing:
     * - the Muay Thai Record
     * - Associative array of all other disciplines

     * @return array all disciplines
     */
    public function sorted_initial_record(){

        $initial_record = $this->initial_record;
        $sorted_record = array();

        if($initial_record):
            $muay_thai_record = "";
            foreach ($initial_record as $key => $record) {
                if($record['discipline'] == "Muay Thai"):
                    $muay_thai_record = $record;
                    unset($initial_record[$key]);
                endif;
            }

            $sorted_record["muay_thai"] = $muay_thai_record;
            $sorted_record["other_disciplines"] = $initial_record;

            return $sorted_record;
        endif;
    }

    /**
     * Receives an associative array of all uncounted
     * for athlete across all disciplines, then isolates
     * the records into one associative array containing:
     * - the Muay Thai Record
     * - Associative array of all other disciplines

     * @return array all disciplines
     */
    public function sorted_uncounted_record(){

        $uncounted_record = $this->uncounted_experience;
        $sorted_record = array();

        if($uncounted_record):
            $muay_thai_record = "";
            foreach ($uncounted_record as $key => $record) {
                if($record['discipline'] == "Muay Thai"):
                    $muay_thai_record = $record;
                    unset($uncounted_record[$key]);
                endif;
            }

            $sorted_record["muay_thai"] = $muay_thai_record;
            $sorted_record["other_disciplines"] = $uncounted_record;

            return $sorted_record;
        endif;
    }

    /**
     * Tally's the total of a wins/loss/draw record for
     * an athlete
     * @param  string $column wins/loss/draw/demo
     * @return integer         total wins/loss/draw/demo
     */
    public function tally_record_column($column){
        $initial_record = $this->initial_record;

    
        if($initial_record):
            $record = array_column($initial_record, $column);
            $total_for_column = array_sum($record);
            return $total_for_column;
        endif;
    }

    /**
     * Tally's the total uncounted wins/loss/draw record for
     * an athlete
     * @param  string $column wins/loss/draw/demo
     * @return integer         total wins/loss/draw/demo
     */
    public function tally_uncounted_record_column($column){
        $uncounted_experience = $this->uncounted_experience;
        $record = array_column($uncounted_experience, $column);
        $total_for_column = array_sum($record);
        return $total_for_column;
    }

    /**
     * Returns athelets record in format win-loss-draw
     * Grabs values from acf custom fields
     * @param  var $id The althlete id from $post->ID
     * @return string     Formated record
     */
    public function athlete_record(){
        $wins = count($this->fights_won()) + $this->tally_record_column("wins");
        $losses = count($this->fights_lossed()) + $this->tally_record_column("loss");
        $draws = count($this->fights_drawn()) + $this->tally_record_column("draw");
        $demo="";
        if($this->tally_record_column("demo"))
            $demo = ", " . $this->tally_record_column("demo") . " Demo";

        return "$wins-$losses-$draws$demo";
    }

    /**
     * Returns total number of fights by fighter
     * Grabs values from acf custom fields
     * @param  var $id The althlete id from $post->ID
     * @return int     total fights
     */
    public function total_fights(){
        $wins = count($this->fights_won()) + $this->tally_record_column("wins");
        $losses = count($this->fights_lossed()) + $this->tally_record_column("loss");
        $draws = count($this->fights_drawn()) + $this->tally_record_column("draw");
        $demo= $this->tally_record_column("demo");

        return $wins+$losses+$draws+$demo;
    }

    /**
     * Get human readible athlete upgrade
     * @return string athlete upgrade
     */
    public function show_athlete_upgrade(){
        $field = $this->athlete_upgrade_object;
        $value = $this->athlete_upgrade;
        $label = $field['choices'][$value];
        return $label;
    }


    /**
     *
     * Bout information
     *
     */
    

    /**
     * return array of all bouts for this fighter
     *
     * Uses php closures 'use', see php.net/manual/en/class.closure.php
     * http://www.stetsenko.net/2012/02/php-array_filter-with-arguments/
     * 
     * @return array bouts cpt
     */
    public function get_bouts(){
        $id = $this->ID;
        $all_bouts = cgp_get_all_bouts();
        $my_bouts = array_filter($all_bouts, function($v) use ($id){
             return array_key_exists($id, $v->fighters_points);
          });
        return $my_bouts;
    }

    /**
     * return array of all contact bouts for this fighter
     *
     * Uses php closures 'use', see php.net/manual/en/class.closure.php
     * http://www.stetsenko.net/2012/02/php-array_filter-with-arguments/
     * 
     * @return array bouts cpt
     */
    public function get_full_contact_bouts(){
        $id = $this->ID;
        $all_bouts = cgp_get_all_bouts();
        $my_bouts = array_filter($all_bouts, function($v) use ($id){
             return array_key_exists($id, $v->fighters_points);
          });
        return $my_bouts;
    }

    /**
     * Returns array of fights won by fighter
     *
     * Uses php closures 'use', see php.net/manual/en/class.closure.php
     * http://www.stetsenko.net/2012/02/php-array_filter-with-arguments/
     *
     * 
     * @param  int $fighter_id user ID
     * @var array all bouts
     * @return array             won fights
     */
    public function fights_won(){
        $fighter_id = $this->ID;
        // array of all bouts
        $all_bouts = $this->get_bouts();

        
        // return filtered array of bouts that has the user's id under the win key
          $won_bouts = array_filter($all_bouts, function($v) use ($fighter_id){
             return $v->win == $fighter_id;
          });
        // return  the array
        return $won_bouts;
    }

    /**
     * Get fights won at MTO sanctioned events
     * @return array bouts cpt
     */
    public function get_mto_wins(){
        $fighter_id = $this->ID;
        // array of all bouts
        $all_wins = $this->fights_won();
        
        
        // return filtered array of bouts that has the user's id under the win key
          $mto_wins = array_filter($all_wins, function($v) use ($fighter_id){
             return $v->organization == "MTO";
          });
        // return  the array
        return $mto_wins;
    }


    /**
     * Returns array of fights lossed by fighter
     *
     * Uses php closures 'use', see php.net/manual/en/class.closure.php
     * http://www.stetsenko.net/2012/02/php-array_filter-with-arguments/
     *
     * 
     * @param  int $fighter_id user ID
     * @var array all bouts
     * @return array             lossed fights
     */
    public function fights_lossed(){
        $fighter_id = $this->ID;
        // array of all bouts
        $all_bouts = $this->get_bouts();
        
        // return filtered array of bouts that has the user's id under the win key
          $lossed_bouts = array_filter($all_bouts, function($v) use ($fighter_id){
             return $v->lose == $fighter_id;
          });
        // return the array
        return $lossed_bouts;
    }


    /**
     * Get fights lossed at MTO sanctioned events
     * @return array bouts cpt
     */
    public function get_mto_losses(){
        $fighter_id = $this->ID;
        // array of all bouts
        $all_losses = $this->fights_lossed();
        
        
        // return filtered array of bouts that has the user's id under the win key
          $mto_losses = array_filter($all_losses, function($v) use ($fighter_id){
             return $v->organization == "MTO";
          });
        // return  the array
        return $mto_losses;
    }

    /**
     * Returns all fights drawn by fighter
     *
     * Uses php closures 'use', see php.net/manual/en/class.closure.php
     * http://www.stetsenko.net/2012/02/php-array_filter-with-arguments/
     *
     * 
     * @param  int $fighter_id user ID
     * @var array all bouts
     * @return array             drawn fights
     */
    public function fights_drawn(){
        $fighter_id = $this->ID;
        // array of all bouts
        $all_bouts = $this->get_bouts();
        
        // return filtered array of bouts that has the user's id under the win key
          $drawn_bouts = array_filter($all_bouts, function($v) use ($fighter_id){
            if($v->draw)
             return in_array($fighter_id, $v->draw);
             
          });
        // return a count of number of items in the array
        return $drawn_bouts;
    }


    /**
     * Get fights drawn at MTO sanctioned events
     * @return array bouts cpt
     */
    public function get_mto_draws(){
        $fighter_id = $this->ID;
        // array of all bouts
        $all_draws = $this->fights_drawn();
        
        
        // return filtered array of bouts that has the user's id under the win key
          $mto_draws = array_filter($all_draws, function($v) use ($fighter_id){
             return $v['organization'] == "MTO";
          });
        // return  the array
        return $mto_draws;
    }

    /**
     * Get all unique weight classes fighter has competed in
     *
     * Takes optional $athlete_experience_class, if set will find weight classes
     * that match $athlete_experience_class. (handy for fighted the weight classes 
     * a fighter competed in at the specified weight category.) 
     * Else, if not set, all weight classes
     * will be returned.
     *
     * @param  string $athlete_experience_class Athlete's current experience class
     * @return array weight class id as key and weight class name as value
     */
    public function fought_in_weight_classes($athlete_experience_class=null){
        $my_bouts = $this->get_bouts();


       $weight_classes = array();
       foreach ($my_bouts as $bout) {

        //do not include bout in calculation if it is a catchweight
        if(strtolower($bout->type)=='catchweight')
            continue;

        if (strtoupper($bout->experience_class) == strtoupper($athlete_experience_class) 
            OR $athlete_experience_class == null):
            // add to array
            $weight_class = $bout->weight[0];
            $weight_classes[$weight_class->ID] = $weight_class->post_title;
        endif;
       }

       return $weight_classes;
    }

    /**
     * Gets ranking for athlete in each weight division they competed in
     * or returns the championship the user holds for that division
     * 
     * @return array contains array of information to be printed
     */
    public function rankings_for_weightclasses(){
            //array with key=>value as weight class id => weight class title
                $experience_class = strtoupper($this->experience_class());
               $weight_classes = $this->fought_in_weight_classes($experience_class);
              
               $rankings = array();
               $championships = $this->current_championships();

           foreach ($weight_classes as $weight_id => $weight_title):
                $user_info = array();
               // set up sort parameters
               $user_info['user_organization']= "MTO";
               $user_info['user_gender'] = $this->gender;
               $user_info['user_age'] = $this->age_category();
               $user_info['user_experience']=strtoupper($this->experience_class());
               $user_info['user_weight_id'] = $weight_id;
               $user_info['user_weight_title'] = $weight_title;
               $user_info['id'] = $this->ID;
               $user_info['championships'] = $championships;

                // get all championships that match division criteria
                // as filtered in the get_athlete_championship_title function
                $championship = get_athlete_championship_title($user_info);
                
                
                // if there is a championship for this division, use the title
                // as the user rank, otherwise do the math to find the user's rank
                // in the division
                if($championship):
                    //get the first championship in the array
                    $first_championship = array_shift($championship);
                    //assign the title to user rank
                    $user_info['rank'] = $first_championship['title'];
                else:

                    $user_info['rank'] = get_athlete_rank_in_division($user_info);
                endif;
               

               array_push($rankings, $user_info);
              
              
            endforeach;

            return $rankings;
    }


    public function age_classes_fought_in(){
        $bouts = $this->get_bouts();

        // array of all age categories fighter competed
        $age_categories_competed = array();
        foreach($bouts as $bout):
            if(!in_array($bout->age_category, $age_categories_competed )):
                array_push($age_categories_competed, $bout->age_category);
            endif;
        endforeach;

        return $age_categories_competed;
    }
    /**
     * return a list of all experience classes athlete competed
     * in. The keys of the array corresponds with the keys of
     * the experience class it is associated with
     * @return array list of experience classes competed
     */
    public function experience_classes_fought_in(){
        $bouts = $this->get_bouts();
        $experience_classes = get_experience_class_keys(); // array of age classes and their keys
       
        
        // array of all experience classes fighter competed
        $experience_class_competed = array();
        foreach($bouts as $bout):
            if(!in_array($bout->experience_class, $experience_class_competed )):
                $experience_cat_key = array_search(strtolower($bout->experience_class), $experience_classes);
                $experience_class_competed[$experience_cat_key] = $bout->experience_class;
            endif;
        endforeach;

        return $experience_class_competed;
    }


    /**
     * Return an array of current championships held by
     * athlete
     * @return array current championships
     */
    public function current_championships(){
        $championships = $this->championships;
        $current_championships = array();
        if($championships):
            foreach ($championships as $championship) :
              if( empty($championship['end_date']) OR 
                strtotime($championship['end_date']) > strtotime('now') ):
                    array_push($current_championships, $championship);
              endif;
            endforeach;
        endif;

        return $current_championships;
    }




}



 ?>