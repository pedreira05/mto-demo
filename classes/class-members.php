<?php 

/**
* Defines attributes of all members
*/
class mtoMembers
{

    public $class_id;
    public function __construct($id)
    {
        
        $this->first_name = get_user_meta($id, 'first_name', true);
        $this->last_name = get_user_meta($id, 'last_name', true);
        /**
         * Get Role, and unserialize it if necessary
         * @var [type]
         */
        $role_meta = get_user_meta($id, 'membership_type', true);
        $this->role = (is_serialized( $role_meta)) ? unserialize($role_meta) : $role_meta;

        $this->status = get_user_meta($id, 'status', true);
        $start_date =  get_user_meta($id, 'start_date', true);
        $this->start_date = cgp_format_date($start_date);
        $this->expiry_date = cgp_format_date(get_user_meta($id, 'expiry_date', true));
        $this->dob = get_user_meta($id, 'dob', true);

        /**
         * Get club and unserialize it if necessary
         * @var [type]
         */
        $club_meta = get_user_meta($id, 'club', true);
        $this->club = (is_serialized( $club_meta)) ? unserialize($club_meta) : $club_meta;

        $this->gender = get_user_meta($id, 'gender', true);
        $this->image =  get_user_meta($id, 'image',true);
        $this->weight = apply_filters( 'acf_weight_filter', $id );
        $this->experience_class = get_user_meta($id, 'experience_class', true);
        $this->record = apply_filters( 'cgp_athlete_record_filter',  $id);
        $this->fighter_page = get_permalink( $id );
        $this->city = get_user_meta($id, 'city', true);
        $this->provincestate = get_user_meta($id, 'provincestate', true);
        $this->country = get_user_meta($id, 'country', true);
        $this->description = get_user_meta($id, 'description', true);
        $this->nickname = get_user_meta($id, 'nickname', true);
        $this->ID = $id;
        
    }


    
    /**
     *
     * Filter to echo an image from ACF
     *
     * Takes the image array from this object,
     * runs it through the filter to return an
     * image with the specified size
     *
     * @param string $size custom image size
     * @return html image tag
     *
     */
    public function echo_image($size="content_block"){
        
       echo apply_filters( 'acf_img_filter', $this->image, $size);
        

    }

    /**
     * Get member's age
     * @param  int $id Post Id
     * @return string     age
     */
    public function age(){
        $date1 = new DateTime($this->dob);
        $date2 = new DateTime(date('d-M-Y'));
        $interval = $date1->diff($date2);
        return $interval->y;
    }

    /**
     * Filters an ACF relationship field
     *
     * Finds all the posts in the relationship field and returns the
     * title 
     */
    function get_relationship_field_titles($field){
        //$field is an array of posts
        $items = "";
        if(!empty($field)){
            foreach ($field as $post) {
                if($post->post_title){
                    $items.=trim("$post->post_title");
                } else {
                    $title = get_the_title( $post );

                    $items.=trim($title);
                }

            }
        }
        return $items;
    }

    

    /**
     * List of member's suspension record
     * @return array All suspensions values date, reason, duration
     */
    public function suspensions(){

    }

    
    
    

    
}


 ?>