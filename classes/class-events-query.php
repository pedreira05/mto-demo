<?php 

/**
* Events Query
*/


class events_Query
{
    
    
    function __construct($bout, $event)
    {
        
      $this->title = $bout['title'];
      $this->level = $bout['level'];
      $this->organization = $bout['organization'];
      $this->type = $bout['type'];
      $this->weight = $bout['weight'];
      $this->weight_range = $bout['weight_range'];
      $this->red_member = $bout['red_member'];
      $this->level_of_contact = $bout['level_of_contact'];
      $this->blue_member = $bout['blue_member'];
      $this->red_club = $bout['red_club'];
      $this->blue_club = $bout['blue_club'];
      $this->age_category = $bout['age_category'];
      $this->experience_class = $bout['experience_class'];
      $this->winner = $bout['winner'];
      $this->result_by = $bout['result_by'];
      $this->stoppage_round = $bout['stoppage_round'];
      $this->stoppage_time = $bout['stoppage_time'];
      $this->red_tournament_placement = $bout['red_tournament_placement'];
      $this->blue_tournament_placement = $bout['blue_tournament_placement'];
      $this->red_points = $bout['red_points'];
      $this->blue_points = $bout['blue_points'];
      $this->judge_1 = $bout['judge_1'];
      $this->judge_2 = $bout['judge_2'];
      $this->judge_3 = $bout['judge_3'];
      $this->referee = $bout['referee'];
      $this->timekeeper = $bout['timekeeper'];
      $this->official = $bout['official'];
      $this->video = $bout['video'];
      $this->notes = $bout['notes'];
          
        
        

                // get the id for each fighter
                if(isset($bout['red_member'][0]->ID)){
                  $red_member = $bout['red_member'][0]->ID;
                } else {
                  $red_member = $bout['red_member']['ID'];
                }

                 if(isset($bout['blue_member'][0]->ID)){
                  $blue_member = $bout['blue_member'][0]->ID;
                } else {
                  $blue_member = $bout['blue_member']['ID'];
                }


                // get points for each fighter
                $red_points = $bout['red_points'];
                $blue_points = $bout['blue_points'];

              // add all fighters for this bout to new array key
              $this->fighters_points = array(
                                            $red_member   =>    $red_points, 
                                            $blue_member  =>    $blue_points
                                          );
              $this->fighters_age_category = array(
                      $red_member     =>   get_user_meta( $red_member, 'calculated_age_category' ),
                      
                      $blue_member     =>    get_user_meta( $blue_member, 'calculated_age_category' )
                );

                /**
                 *
                 * Get result of the bout and append to bout information
                 *
                 */
                
                $winner = $bout['winner'];

                // make sure variables are empty
                $win = "";
                $lose = "";
                $draw = "";

                // assign red id to 'winner' if they won
                if("Red Athlete" == $winner):
                  $win = $red_member;
                  $lose = $blue_member;
                // assign blue id to 'winner' if they won
                elseif("Blue Athlete" == $winner):
                  $win = $blue_member;
                  $lose = $red_member;
                // assign both fighter's ids to 'draw' if they draw
                elseif("Draw" == $this->result_by):
                  $draw = array($red_member, $blue_member);
                endif;


              

              // append win/lose/draw info to array
              $this->win = (int) $win;
              $this->lose = (int) $lose;
              $this->draw = $draw;


              //add event title to bouts
              $this->event  = $event->post_title;

              $this->ID  = $event->ID;

              $this->url  = get_permalink($event->ID );



              //add event end date to bouts
              $this->event_end_date = $event->EventEndDate;


              //add a gender based on gender of red member
              $gender = get_user_meta( $red_member, 'gender' );
              if(isset($gender[0])){
                $this->gender = $gender[0];
              } else {
                $this->gender = $gender;
              }

              /**
               * Get weight class of bout   
               */
              $type = $bout['type'];
              if($type =='Catchweight'){

                // if catchweight is selected,
                // get the weight range from the weight range field
                $weight_range = $bout['weight_range'];
              } else {

                // in all other cases, get weight from weight field
                $weight_range = $bout['weight'];
              }
              
              //parse weight range and format it based on what type of
              //object it is
              $this->weight_range = $weight_range;
             
    }


}

 ?>