<?php 

/**
 *
 * Archive filters
 *
 * true
 *
 */
$gender = array(
        'Male',
        'Female'
    );
$age_category = get_field_object('field_5727f49ceed3b');
$experience_class = get_field_object('field_5727f9ba3858e');
$status = array(
    'Active',
    'Inactive'
    );
 ?>  

 <div class="filters-wrap row">
    <div class="col-md-8">
        <input type="hidden" class="filter-info" name="filter-id" value="athlete-filter">
        <div id="archive-filters">
            Filter by: <br>
            <select name="gender" id="athlete_gender" class="filter_option  gender">

                <option value="" label="" selected="selected">All Genders</option>
                <?php foreach($gender as $gender): ?>
                <option value="<?php echo $gender; ?>"><?php echo ucfirst($gender); ?></option>
            <?php endforeach; ?>

        </select>
        <select name="age_category" id="athlete_age_category" class="filter_option  age_category">

            <option value="" label="" selected="selected">All Age Categories</option>
            <?php foreach($age_category['choices'] as $age_key => $age_category): ?>
            <option value="<?php echo $age_category; ?>"><?php echo ucfirst($age_category); ?></option>
        <?php endforeach; ?>
    </select>
   
<select name="status" id="athlete_status" class="filter_option  status">

    <option value="" label="" selected="selected">Status</option>
    <?php foreach($status as $status): ?>
    <option value="<?php echo strtolower($status); ?>"><?php echo ucfirst($status); ?></option>
<?php endforeach; ?>
</select>
<input type="number" id="athlete_weight_lbs" name="weight_range" class="filter_option  weight_kg" placeholder="Weight" style="max-width: 90px">

<button class=" search_trigger filter_trigger btn btn-default">Filter</button>
<a href="#" class="clear_filters">Clear All</a>

</div>
</div><!-- col-md-10 -->
<div class="col-md-4 text-right text-xs-center">
    <div class="fuzzy-search-wrap ">
        <form>
            <span style="text-align:left; display:inline-block; width: 100%">OR:</span> <br>
            <input class="athlete-list-search"  placeholder="Search" style="margin-bottom:0" />
            <button class="fuzzy-search-button search_trigger filter_trigger btn btn-default">Search</button>
        </form>
    </div>
</div><!-- col-md-2 -->

</div><!-- row -->

<div class="table-responsive gutter athletes" id="athletes"><!-- row -->

    <table id="athlete-list-table" class="table table-condensed athlete-list  cf">

        <thead>
            <tr>
                <th class="tbl-head">
                    <button class="sort" data-sort="first-name"> Name </button>
                </th>
                <th class="tbl-head">
                    <button class="sort" data-sort="status"> Status </button>
                </th>
                <th class="tbl-head">
                    <button class="sort" data-sort="expiry-date"> Expiry </button>
                </th>
                <th class="tbl-head">
                    <button class="sort" data-sort="gender"> Gender </button>
                </th>
                <th class="tbl-head">
                    <button class="sort" data-sort="city"> City </button>
                </th>
                <th class="tbl-head">
                    <button class="sort" data-sort="state"> Region </button>
                </th>
                <th class="tbl-head">
                    <button class="sort" data-sort="country"> Country </button>
                </th>
                <th class="tbl-head">
                    <button class="sort" data-sort="age-category"> Age </button>
                </th>
                <th class="tbl-head">
                    <button class="sort" data-sort="weight-range"> Weight Range (kg) </button>
                </th>
                <th class="tbl-head">
                    <button class="sort" data-sort="club"> Club </button>
                </th>
            </tr>
        </thead>
        <tbody class="list athlete-list-rows athletes">

            
        </tbody>
    </table>
</div><!-- row end -->

<div class="loading" style="width:200px; margin:0 auto; display:none">
    <img src="<?php echo plugins_url( ); ?>/fighter-cards/images/spinner.gif" alt="loading" class="load-img">
</div>
