

(function($) {
    /**
     *
     * Filtering for athlete archive page
     *
     */
    
    // set all search inputs to blank on page load
    $('.filter_option').val("");

    //-----------------------------------
    // Filter search results whenever an input is updated
    // 
    function append_more_results(){
        $(".athlete-list-rows").append('<button class="load_more">Load more</button');
          
          
    }
    

    function filter_results(theOffset){
        

        var args = {};
        $('.filter_option').each(function(){

            // vars
            var filter = $(this).attr('name');
            var vals = $(this).val();
            
            args[ filter ] = vals;
            
        });

        

            // get page number for pagination
            var res = theOffset;
            var page_num = String(res).match(/(\d+)/g);
            var fuzzy_search = $('.athlete-list-search').val();
            var member_type = $('input[name=member_type]').val();


            // kluge to fix tense descrepency between member type values saved in
            // database and those transmitted
            if(member_type == "athletes"){
                member_type = "athlete";
            }
            if(member_type == "coaches"){
                member_type = "coach";
            }
            if(member_type == "officials"){
                member_type = "official";
            }

            // get id of the form that sent the filter
            var filterID = $('.filter-info').val();

            var theAction = "";
            if (filterID == "rankings-filter"){
                theAction = 'ajax-athlete_ranking_filter';
            }
            if (filterID == "athlete-filter"){
                theAction = 'ajax-athlete_archive_filter';
            }

            
            
            if(theAction == "") 
                return ;
            
            $.ajax({
                type: 'POST',
                dataType: 'JSON',
                url: PT_Ajax.ajaxurl,
                data:{
                    action: theAction,
                    filters: args,
                    paged: Number(page_num),
                    fuzzy: fuzzy_search,
                    offset: Number(theOffset),
                    filter_id: filterID,
                    memberType: member_type,
                    nextNonce : PT_Ajax.nextNonce
                },
                success: function(filters){
                    
                    
                    //setup pagination variables
                    var nextPage = filters.next_page;
                    var prevPage = filters.previous_page;
                    var currentPage = filters.current_page;
                    var maxPages = filters.max_num_pages;

                  

                    // pagination html
                    if(nextPage != "last-page"){
                        
                        $(document).find('.next-button').removeClass('hidden');
                    } else {
                        $(document).find('.next-button').addClass('hidden');

                    }
                    $(document).find('.next-button').data('pager', nextPage);

                    if(prevPage != "last-page"){
                        $(document).find('.prev-button').removeClass('hidden');
                    } else {
                        $(document).find('.prev-button').addClass('hidden');

                    }
                    $(document).find('.prev-button').data('pager', prevPage);

                    $("input[name='query_paged']").val(currentPage).attr('max', maxPages).attr('min', '1');
                    


                    // upon success, remove current data in the table
                    // and add the filtered results. If there are no
                    // filtered results and a 'no results found' 
                    // message
                    
                  
                    if($.trim(filters.members) !== ""){
                        $(".athlete-list-rows").append(filters.members);
                    } else{
                        $(".athlete-list-rows").html("<tr><td colspan='12'>No fighters matching your criteria found. Please broaden your search and try again</td></tr>");
                    }
                    
                    

                  
                    
                    var queryStatus = filters.query_status;

                    $("input[name='query_status']").val(queryStatus);

                    $(document).find('.query_paged_wrap').html(' of ' + filters.max_num_pages);


                },
                complete: function() {
                    $(".loading").hide();

                    $(document).find('.numerical_pager').prop('disabled', false);
                    $(document).find('.pagination').removeClass('disabledbutton');


                    // var queryStatus = parseInt($(document).find('input[name=query_status]').val());

                    // var queryOffset = parseInt($(document).find('input[name=query_offset]').val());
                    // var offset = queryOffset + 10;


                    // if(queryStatus === 1){
                    //     append_more_results();
                    // }
                 },
                beforeSend: function(){
                    $(document).find('.numerical_pager').prop('disabled', true);
                    $(document).find('.pagination').addClass('disabledbutton');
                   
                    // prepare screen: show that page is load
                    $(".athlete-list-rows").empty();
                    $(document).find(".loading").show();
                },
                error: function(){
                }
            });

     
      return false;
      }

     


    $(document).ready(function(){

        filter_results(0);
    });

    $('.filter_trigger').on('change', function(){
        filter_results(0);
    });

    $('.search_trigger').on('click', function(e){
        e.preventDefault();
        
        filter_results(0);
    });

    $(document).on('click', '.load_more', function(e){
        e.preventDefault();
        var queryOffset = parseInt($(this).data('pager'));
        filter_results(queryOffset);
    });
    $(document).on('change', '.numerical_pager', function(e){
        
        var queryOffset = parseInt($(this).val() * 10 - 10);
        filter_results(queryOffset);
    });
    $(document).on('click', 'a.page-numbers', function(e){
        var page = $(this).attr('href');
        filter_results(page);
    });
    $('.clear_filters').click(function(e){
        e.preventDefault();

        $('.filter_option').prop("selectedIndex", 0).val('');
        $('.athlete-list-search').val('');
        filter_results(0);
    });

    // prevent a number outside the scope of existing pages from being entered
    // in the pagination number box
    
    $(document).on('keypress', '.numerical_pager', function(e){
        if(e.which == 13){

            var maxPages = $(this).attr('max');
            var minPages = $(this).attr('min');

            var theNum = $(this).val();

            

            if(theNum > maxPages){
                $(this).val(maxPages);
            }
            if(theNum < minPages){
                $(this).val(minPages);
            }
            return;
        }
    });


    $('.claim-profile-link').click(function(e){
        e.preventDefault();
        $('#claim-profile').show();
    });

    $('.member-type-athletes .member-stats-table .outdated').html('<span>Record Update Required</span>');



})(jQuery);