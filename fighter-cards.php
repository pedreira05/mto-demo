<?php

/*
Plugin Name: Fighter Cards
Plugin URI: http://www.curtispeters.ca
Description: Manage fighter cards for Muay Thai Ontario
Version: 1.0
Author: Curtis Peters
Author URI: http://www.curtispeters.ca
License: #
*/


/**
 *
 * Custom functions
 *
 */
define( 'MY_PLUGIN_PATH', plugin_dir_path( __FILE__ ) );
define( 'CGP_TEMPLATE_PATH', plugin_dir_path( __FILE__ ) .'/template-parts/' );

/**
 * Object Oriented Programming
 *
 * Code in these files give specific examples of OOP
 */
require plugin_dir_path(__FILE__) . '/classes/class-members.php';
require plugin_dir_path(__FILE__) . '/classes/class-athlete.php';
require plugin_dir_path(__FILE__) . '/classes/class-events-query.php';

/**
 * Other functions
 */
require plugin_dir_path(__FILE__) . '/functions/theme-setup.php';
require plugin_dir_path(__FILE__) . '/functions/custom-post-types.php';
require plugin_dir_path(__FILE__) . '/functions/filters.php';
require plugin_dir_path(__FILE__) . '/functions/functions.php';
require plugin_dir_path(__FILE__) . '/functions/member-settings.php';
require plugin_dir_path(__FILE__) . '/functions/overwrite-themes-plugins.php';
require plugin_dir_path(__FILE__) . '/functions/record-display-functions.php';
require plugin_dir_path(__FILE__) . '/functions/cron-job.php';
require plugin_dir_path(__FILE__) . '/functions/acf-user-role-field-setting.php';
// require plugin_dir_path(__FILE__) . '/functions/init.php';



/**
 *
 * Styles and scripts
 *
 */

add_action('admin_enqueue_scripts', function() {
    // admin js
    wp_register_script( 'fighter-cards-script', plugin_dir_url( __FILE__ ) . 'js/fighter-cards-admin.js', array('jquery'), '20180802', 'true' );
  // enqueing:
  wp_enqueue_script( 'fighter-cards-script' );
  wp_enqueue_style( 'admin-screens', plugin_dir_url( __FILE__ ) . 'css/admin-screens.css', false, '20171212', 'all' );

  //MTO Members only
  if(current_user_can( 'member' )){
    wp_enqueue_style( 'member-admin-screens', plugin_dir_url( __FILE__ ) . 'css/member-admin-screen.css', false, '20200118', 'all' );
    wp_enqueue_script( 'fighter-card-member-amdin', plugin_dir_url( __FILE__ ) . 'js/min/fighter-cards-members-admin-min.js', array('jquery'), '20191219', 'true' );
    

  }

});

add_action('wp_enqueue_scripts', function() {
    // front-end css
    wp_enqueue_style('cgp-styles', plugin_dir_url( __FILE__ ) . 'css/fighter-plugin-styles.css', false, '20200227', 'all');

  wp_enqueue_script( 'frontend_fighter', plugin_dir_url( __FILE__ ) . 'js/min/fighter-cards-frontend-min.js',  array('jquery'), '20200118', true );
  


  if(is_page_template( $template = 'page-members.php' )){
    wp_enqueue_script( 'members-page-js', plugin_dir_url( __FILE__ ) . 'js/min/member-profile-page-min.js',  array('jquery'), '20191228', true );
   
    // bootstrap styles
    wp_enqueue_style('bootstrap-styles', 'https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css', false, '4.4.1', 'all');
    //bootstrap script
    wp_enqueue_script( 'bootstrapjs', 'https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js',  array('jquery'), '4.4.1', true );
    //bootstrap bundle
    wp_enqueue_script( 'bootstrap-bundle-js', 'https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js',  array('jquery'), '4.4.1', true );

  }



},80);    


add_action( 'init', function(){
    wp_enqueue_style( 'cgp-styles' );

 } );
 ?>
