## Intro

This plugin is a custom CMS written to manage the records of professional Muay Thai fighters. It allows fighters to register and manage their own records, as well as for coaches and administrators to overwrite updates made by the fighters.

For this demo, I've included the parts of the plugin responsibile for recording, calculating and displaying a fighter's record and removed contributions from other programmers so you can get a better sense of my code without too much hunting around. I managed this project for 3 years so there is growth admidst the consistency in my code.

## Highlights

1. Start with fighter-cards.php to find the list files containing OOP.
2. Review fighter-cards-frontend.js for the script that, via ajax, retreives the objects and displays it to the reader
3. The functions in the /functions folder are all examples of the advanced coding to either initiate the plugin or perform other custom actions on the website